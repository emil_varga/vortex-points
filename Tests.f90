module Tests
  use Time_evolution

  contains

    subroutine boundary_vel(prefix, vort_circ, vort_pos)
      integer, dimension(:), intent(in) :: vort_circ
      real, dimension(:,:), intent(in)    :: vort_pos
      character(len=*), intent(in)      :: prefix

      integer, parameter            :: points = 100
      integer, parameter            :: west = 1, south = 2, east = 3, north = 4
      real, dimension(2, points, 4) :: boundaries !boundary points, second index selects side, third the component
                                                  !x -> 1, y -> 2
      real, dimension(2, points, 4) :: boundary_velocities
      integer                       :: i, j
      real                          :: pos
      
      boundaries(1,:,west)  = 0
      boundaries(2,:,south) = 0
      boundaries(1,:,east)  = 1
      boundaries(2,:,north) = 1

      do i=1,points
         pos = (1.0*i)/points
         boundaries(2,i,west)  = pos
         boundaries(1,i,south) = pos
         boundaries(2,i,east)  = pos
         boundaries(1,i,north) = pos
      end do
      
      do i=1,4
         call compute_velocities(boundaries(:,:,i), vort_circ, vort_pos, boundary_velocities(:,:,i))
      end do

      open(unit=1, file=prefix//'boundary_velocity.dat', status='NEW')
      
      do i=1,points
         write (1,*) boundaries(2,i,west), (boundary_velocities(:, i, j), j=1,4)
      end do
      
      close(1)

    end subroutine boundary_vel
    
end module Tests
