module Reconnections
  implicit none

  contains

    recursive function reconnect_simple(vort_pos, vort_circ, rec_l, recs,&
         &anih, anih_start_i, xwallp) result(rec)
      implicit none
      integer, dimension(:), intent(inout)              :: vort_circ
      integer, dimension(:), intent(out), optional      :: recs
      real, dimension(:,:), intent(inout)               :: vort_pos
      real, intent(in)                                  :: rec_l
      !anih - anihilate?, xwallp - walls in x direction?
      logical, intent(in), optional                     :: anih, xwallp
      !anihilate only vortices with index > i
      integer, intent(in), optional                     :: anih_start_i 
      integer :: rec, i, j, N, R
      real    :: r2, r2rec, dx, dy
      logical :: anihilate = .false., wallp=.false., too_close=.false.

      if(present(anih)) anihilate = anih
      if(present(xwallp)) wallp = xwallp

      rec = 0
      r2rec = rec_l**2
      if(present(recs)) then
         recs = 0
         R = size(recs)
      end if
      N = size(vort_circ)

      ! anihilation with walls
      if(wallp) then
         do i=1,N
            dx = min(vort_pos(1,i), 1-vort_pos(1,i));
            if(dx < rec_l .and. vort_circ(i)/=0) then
               if(.not.anihilate) then
                  call random_number(vort_pos(:,i))
               else
                  if(present(anih_start_i)) then 
                     if(i > anih_start_i) then
                        vort_circ(i) = 0
                     else
                        call random_number(vort_pos(:,i))
                     end if
                  else
                     vort_circ(i) = 0
                  end if
               end if
               rec = rec + 1
               if(present(recs)) then
                  recs(2*rec) = i
               end if
            end if
         end do
      end if
      
      !vortex-vortex anihilation
      do i=1,N
         do j=(i+1),N
            if((vort_circ(j) /= 0) .and. (vort_circ(i) /= 0)) then
               dx = abs(vort_pos(1,i) - vort_pos(1,j))
               dy = abs(vort_pos(2,i) - vort_pos(2,j))
               dx = min(dx, 1-dx); dy = min(dy, 1-dy)
               r2 = dx**2 + dy**2
               too_close = (r2 < r2rec) .and. (vort_circ(i) == -vort_circ(j))
               if(too_close) then
                  if(.not.anihilate) then
                     call random_number(vort_pos(:,i))
                     call random_number(vort_pos(:,j))
                  else
                     if(present(anih_start_i)) then 
                        if(i > anih_start_i) then
                           vort_circ(i) = 0
                        else
                           call random_number(vort_pos(:,i))
                        end if
                        
                        if(j > anih_start_i) then 
                           vort_circ(j) = 0
                        else 
                           call random_number(vort_pos(:,j))
                        end if
                        
                    else
                       vort_circ(i) = 0
                       vort_circ(j) = 0
                    end if
                  end if
                  rec = rec + 1
                  if(present(recs)) then
                     recs(2*rec) = i
                     recs(2*rec - 1) = j
                  end if
               end if
            end if
         end do
      end do

      !to avoid the fatal cases where two vortices would be randomly thrown on top of each other
      if(rec > 0 .and. .not.anihilate) then
         if(present(recs)) then 
            rec = rec + reconnect_simple(vort_pos, vort_circ, rec_l, recs((2*rec+1):R), xwallp=wallp)
         else
            rec = rec + reconnect_simple(vort_pos, vort_circ, rec_l, xwallp=wallp)
         end if
      end if
    end function reconnect_simple

end module Reconnections
