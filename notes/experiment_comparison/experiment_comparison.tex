\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage{graphicx}
\usepackage{datetime}

\title{Preliminary comparison of 2D vortex point simulations with
  visualisation experiments.}
\date{}

\begin{document}
\maketitle
\begin{flushright}
  Emil Varga, Prague, \today
\end{flushright}

In the following figures some basic comparisons of experimental tracer
particle visualization data with two dimensional vortex points
simulations are shown. The source of experimental data is \cite{jfm}.

Figure \ref{fig:mean_v_components} shows mean velocity components and
velocity magnitude of the simulated tracer particles. The
counterflow is in the y-direction with the normal fluid going up
(positive y-direction). Mean $v_y$ is small because the average
includes both particles trapped on the vortices (going down) and free
particles (going up).

Figure \ref{fig:mean_vabs} shows mean velocity magnitudes, now
differentiating whether particles are trapped or not. Free particles
relatively closely follow the velocity of the normal fluid due to the
imposed counterflow. Trapped particles move in absolute sense somewhat
faster than the superfluid component. 

Trapped particles move with vortices without affecting them in any
way, so this is also the mean velocity of the vortices. In addition to
the imposed superfluid velocity of the counterflow, all vortices also
have a mean component of velocity perpendicular to the counterflow due
to the mutual friction. The direction of this velocity is opposite for
the different signs of vortices and cancels out in averages. However,
it doesn't cancel out when calculating magnitudes. This could be the
reason why trapped particles are moving in absolute sense faster than
the superfluid. However, no tests confirming or disproving this have
been made yet.

The mean magnitudes of accelerations, Fig. \ref{fig:mean_A}, show that
observed accelerations in experiments are much smaller than the
calculated ones, regardless of whether the particles are trapped or
not.

Probability distribution functions of superfluid velocity (\emph{not}
inertial tracer particles) are shown in
Fig. \ref{fig:velocity_pdfs}. Superfluid velocity is measured on a
regular 1000$\times$1000 grid. On this grid further coarse-graining is
superimposed and the velocity from the original grid are averaged
inside the squares corresponding to this coarse-grained mesh. The
effect this has on the p.d.f. is illustrated by the figure. The
distribution function gradually changes into a Gaussian shape. The
roughest mesh, 17 by 17, approximately corresponds to the mean
distance between vortices (there are 300 vortices in the computation,
$\sqrt{300}\approx17$.)

The last figure, Fig \ref{fig:flatness}, shows flatness of the for two
different cases of simulation parameter as a function of the
coarse-graining scale. Flatness is defined as $\left\langle\left(\frac{v -
    \bar v}{\mathtt{std}(v)}\right)^4\right\rangle$. The overbar and the angle
brackets denote mean, std is the standard deviation. It is defined for
the individual Cartesian components of the velocity. Flatness of a
Gaussian is 3. The flatness of the calculated distributions approaches
that of a Gaussian distribution. Similar thing is observed in the
experiments.

\begin{figure}
  \centering
  \includegraphics[width=\textwidth, keepaspectratio]{figures/T175meanV}
  \caption{Mean velocity components and magnitudes of tracer particles
    as a function of power to the heater. Straight lines are
    (absolute) values of the velocity of the normal and superfluid
    component. x and y indices denote the component, counterflow is in
    the y direction with normal fluid flowing in the positive
    direction (up). Values without x, y indices are absolute
    magnitudes. The averages are taken over all particles, no
    distinction as to whether the particles are trapped or not is
    made. Temperature is 1.75 K. The sign or mean y velocities is
    negative.}
  \label{fig:mean_v_components}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth, keepaspectratio]{figures/T175Vabs}
  \caption{Mean magnitude of velocity for particles. Three cases are
    considered: average over all particles, over trapped particles
    only and over free particles only. Straight lines as in
    Fig. \ref{fig:mean_v_components}. Temperature is 1.75 K.}
  \label{fig:mean_vabs}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth, keepaspectratio]{figures/T175A}
  \caption{Mean magnitude of particle acceleration. Three cases as in
    Fig. \ref{fig:mean_vabs} are considered, trapped particles, free
    particles and all particles together. Temperature is 1.75 K.}
\label{fig:mean_A}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=\textwidth, keepaspectratio]{figures/T175_Q608_coarse_pdfs}
  \caption{Probabilty density function of x component of superfluid
    particle velocity. Different curves correspond to the different
    degrees of coarse-graining the velocity field, as indicated in the
    legend. The numbers in the legend mean the mesh density
    superimposed on the 1000$\times$1000 mesh. Curves are shifted
    vertically for clarity. Heater power 608 W/m$^2$.}
  \label{fig:velocity_pdfs}
\end{figure}

\begin{figure}
  \includegraphics[width=0.49\textwidth, keepaspectratio]{figures/T175_Q608_flatness}
  \includegraphics[width=0.49\textwidth, keepaspectratio]{figures/T165_Q490_flatness}
  \caption{Flatness calculated for different coarse-grainings of
    velocity p.d.f.s. Left for 1.75 K and 608 W/m$^2$, right for 1.65
    K and 490 W/m$^2$. Dimensionless parameter $\tau$ is defined as
    ratio of the size of the side of the box used for coarse-graining
    and intervortex distance. The horizontal line indicates flatness
    of 3.}
  \label{fig:flatness}
\end{figure}

\begin{thebibliography}{99}
\bibitem{jfm} M. La Mantia, D. Duda, M. Rotter and L. Skrbek
  (2013). Lagrangian accelerations of particles in superfluid
  turbulence. Journal of Fluid Mechanics, 717, R9
  doi:10.1017/jfm.2013.31
\end{thebibliography}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
