module external_flow
  implicit none
  real, parameter :: pi = 3.14159

  !flat profiles
  real, dimension(2) :: flat_evs=0, flat_evn=0
  
  !normal fluid velocity profile parameters
  integer :: vnprof_periods=0 !(2*pi*periods*x)
  real :: vnprof_amplitude=0

  private flat_evs, flat_evn, vnprof_periods, vnprof_amplitude, pi

  interface
     function get_evn_interface(pos) result (vn)
       real, dimension(:, :), intent(in) :: pos
       real, dimension(2, size(pos,2)) ::  vn
     end function get_evn_interface

     function get_evn_ddt_interface(pos) result(dvndt) !time derivative
       real, dimension(:,:), intent(in) :: pos
       real, dimension(2, size(pos,2)) :: dvndt
     end function get_evn_ddt_interface

     function get_evn_convd_interface(pos) result (vdv) !convective derivative
       real, dimension(:,:), intent(in) :: pos
       real, dimension(2, size(pos, 2)) :: vdv
     end function get_evn_convd_interface

     function get_evn_matdiff_interface(pos) result(DvDt) !material derivative
       real, dimension(:,:), intent(in) :: pos
       real, dimension(2, size(pos,2)) :: DvDt
     end function get_evn_matdiff_interface
  end interface
  
!
! interface that outside world should use
!

!using the cosine as default since most of the code uses it
  procedure(get_evn_interface), pointer         :: get_evn => get_evn_cos                  !profile itself  
  procedure(get_evn_ddt_interface), pointer     :: get_evn_ddt => get_evn_ddt_stationary   !time derivative
  !convective derivative
  procedure(get_evn_convd_interface), pointer   :: get_evn_convd => get_evn_convd_cos
  !material derivative 
  procedure(get_evn_matdiff_interface), pointer :: get_evn_matdiff => get_evn_matdiff_base
contains

!
! setters
!

  subroutine set_evn(f)
    procedure(get_evn_interface) :: f
    
    get_evn => f
  end subroutine set_evn
  
  subroutine set_evn_ddt(f)
    procedure(get_evn_ddt_interface) :: f

    get_evn_ddt => f
  end subroutine set_evn_ddt
  
  subroutine set_evn_convd(f)
    procedure(get_evn_convd_interface) :: f

    get_evn_convd => f
  end subroutine set_evn_convd
  
  subroutine set_evn_matdiff(f)
    procedure(get_evn_matdiff_interface) :: f

    get_evn_matdiff => f
  end subroutine set_evn_matdiff

  subroutine set_vsexternal (vsx, vnx)
    real, dimension(2), intent(in) :: vsx
    real, dimension(2), intent(in), optional :: vnx
    
    flat_evs = vsx
    if(present(vnx)) then
       flat_evn = vnx
    end if
  end subroutine set_vsexternal

  subroutine set_cos_vnprofile(per, amp)
    real :: amp
    integer :: per
    vnprof_periods = per
    vnprof_amplitude = amp
  end subroutine set_cos_vnprofile

!
! cosine profile
!

  function get_evn_cos(pos) result (vn) !cosine profile
    real, dimension(:, :), intent(in) :: pos
    real, dimension(2, size(pos,2)) ::  vn

    vn(1,:) = flat_evn(1)
    vn(2,:) = flat_evn(2)
    vn = vn + cos_vnprofile(pos)
  end function get_evn_cos

  function get_evn_convd_cos(pos) result (vdv) !convective derivative
    real, dimension(:,:), intent(in) :: pos
    real, dimension(2, size(pos, 2)) :: vdv, vn

    real :: A
    integer :: p
    A = vnprof_amplitude
    p = vnprof_periods

    vn = get_evn(pos)
    vdv(1,:) = 0
    vdv(2,:) = -vn(1,:)*A*2*pi*p*sin(pos(1,:)*2*pi*p)    
  end function get_evn_convd_cos

!
! parabolic profile
!

  function get_evn_par(pos) result(vn) !parabolic profile
    real, dimension(:, :), intent(in) :: pos
    real, dimension(2, size(pos,2)) ::  vn

    vn(1,:) = flat_evn(1)
    vn(2,:) = flat_evn(2)
    vn = vn + parabolic_vnprofile(pos)
  end function get_evn_par

  function get_evn_convd_par(pos) result(vdv) !convecetive derivative
    real, dimension(:,:), intent(in) :: pos
    real, dimension(2, size(pos, 2)) :: vdv, vn

    real :: A
    A = vnprof_amplitude

    vn = get_evn(pos)
    vdv(1,:) = 0
    vdv(2,:) = vn(1,:)*4*A*(1-2*pos(1,:))
  end function get_evn_convd_par
  
!
! useful for more general profiles
!

!!!this is OK for all profiles, but can be changed if it would be useful
  function get_evn_matdiff_base(pos) result(DvDt) !material derivative
    real, dimension(:,:), intent(in) :: pos
    real, dimension(2, size(pos,2)) :: DvDt
    DvDt = get_evn_ddt(pos) + get_evn_convd(pos)
  end function get_evn_matdiff_base

!!!stationary profile
  function get_evn_ddt_stationary(pos) result(dvndt) !time derivative
    real, dimension(:,:), intent(in) :: pos
    real, dimension(2, size(pos,2)) :: dvndt

    dvndt = 0
  end function get_evn_ddt_stationary

!!! the flat v_s profile
!nothing more than flat evs should ever be needed
  function get_evs(pos) result (vs)
    real, dimension(:,:), intent(in) :: pos
    real, dimension(2, size(pos, 2)) :: vs
    
    vs(1,:) = flat_evs(1)
    vs(2,:) = flat_evs(2)
  end function get_evs


!
! helper functions
!

  function cos_vnprofile(pos, pp, AA)
    real, dimension(:,:) :: pos
    real, optional :: pp, AA
    real, dimension(2, size(pos, 2)) :: cos_vnprofile
    real :: p, A

    if(present(pp)) then
       p = pp
    else
       p = vnprof_periods
    end if
    if(present(AA)) then
       A = AA
    else
       A = vnprof_amplitude
    end if

    cos_vnprofile(1,:) = 0;
    cos_vnprofile(2,:) = A*cos(pos(1,:)*2*pi*p)
  end function cos_vnprofile

  function parabolic_vnprofile(pos) result(vy)
    real, dimension(:,:), intent(in) :: pos
    real, dimension(2, size(pos,2)) :: vy
    
    vy(1,:) = 0
    vy(2,:) =  4*vnprof_amplitude*pos(1,:)*(1-pos(1,:))
  end function parabolic_vnprofile

end module external_flow
