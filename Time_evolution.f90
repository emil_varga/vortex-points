module Time_evolution
  use Vortices
  use Particles
  use external_flow
  use stepping
  implicit none

contains

    subroutine particle_vort_step_dx(dtinit, particles, trapped, vort_pos, vort_circ,&
         &dxvort, dxpart, vort_v, part_v, dtused, delta_vv, delta_vp)

      real, dimension(:,:), intent(in)  :: particles, vort_pos
      integer, dimension(:), intent(in) :: vort_circ, trapped
      real, intent(in)                  :: dtinit
      real, dimension(size(particles,1), size(particles,2)), intent(out) :: dxpart, part_v
      real, dimension(size(vort_pos,1), size(vort_pos,2)), intent(out)   :: dxvort, vort_v
      real, intent(out)           :: dtused
      real, intent(out), optional :: delta_vv, delta_vp
      
      !tmp contains positions of particles and positions and velocities of particles
      !tmpvel contains their time derivatives
      real, dimension(size(vort_pos,1), size(particles,2) + size(vort_pos,2)) :: tmp, tmpvel, tmpdx
      integer :: V, P
      real :: tmpdelta_vv, tmpdelta_pv, tmpdelta, dt
      V = size(vort_pos,2) !number of vortices
      P = size(particles,2)/2 !number of particles - particle vector contains _both_ positions
                              !and velocities, hence the /2
      
      call compute_vortex_velocities(vort_pos, vort_circ, tmpvel(:,1:V), tmpdelta_vv)
      vort_v = tmpvel(:,1:V)

      tmpvel(:,(V+1):(V+2*P)) = particle_rhs(particles, trapped, vort_pos, tmpvel(:,1:V),&
                                            &vort_circ, tmpdelta_pv)
      part_v = tmpvel(:,(V+1):(V+2*P))

      tmp(:,1:V)                     = vort_pos
      tmp(:,(V+1):(V+2*P))           = particles
      tmpdelta                       = min(tmpdelta_vv, tmpdelta_pv)
      if(present(delta_vv)) delta_vv = tmpdelta_vv
      if(present(delta_vp)) delta_vp = tmpdelta_pv
      
      dt     = choose_timestep_maxvel(dtinit, tmpvel(:,1:(V+P)), tmpdelta)
      dtused = dt
      tmpdx  = rk4_step_dumb(dt, tmp, f, tmpvel)

      dxvort = tmpdx(:,1:V)
      dxpart = tmpdx(:,(V+1):(V+2*P))

      contains

        function f(x)
          real, dimension(:,:) :: x
          real, dimension(size(x,1), size(x,2)) :: f
          call compute_vortex_velocities(x(:,1:V), vort_circ, f(:,1:V))
          f(:,(V+1):(V+2*P)) = particle_rhs(x(:,(V+1):(V+2*P)), trapped, x(:,1:V), f(:,1:V), vort_circ)
        end function f
              
    end subroutine particle_vort_step_dx

end module Time_evolution
