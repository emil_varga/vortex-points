module LinkedList

  type list
     type(list), pointer :: next
     integer :: i
  end type list

  contains

    function cons(data, lst) result (ll)
      integer, intent(in) :: data
      type(list), pointer :: lst
      type(list), pointer :: ll

      allocate(ll)
      ll%next => lst
      ll%i = data
    end function cons

    subroutine add_unique(data, lst)
      integer, intent(in) :: data
      type(list), pointer :: lst, next

      do
         if(.not.associated(lst)) exit
         if(lst%i == data) exit
         if(.not.associated(lst%next)) then
            allocate(next)
            next%i = data
            lst%next => next
            exit
         end if
         lst => lst%next
      end do
    end subroutine add_unique

    subroutine remove_next(lst)
      type(list), pointer :: lst, tmp

      tmp => lst%next
      lst%next => lst%next%next
      deallocate(tmp)
    end subroutine remove_next
      
    function remove_top_list(lst) result(data)
      integer :: data
      type(list), pointer :: lst, tmp
      data = lst%i
      tmp => lst
      lst => lst%next
      deallocate(tmp)
    end function remove_top_list

    function new_list(data) result (nl)
      integer, intent(in) :: data
      type(list), pointer :: nl
      nl => cons(data, NULL())
    end function new_list

    subroutine delete_list(lst)
      type(list), pointer :: lst, tmp
      do
         if(.not. associated(lst)) exit
         tmp => lst
         lst => lst%next
         deallocate(tmp)
      end do
    end subroutine delete_list

    function new_list_from_vector(vec) result(nl)
      integer, dimension(:), intent(in) :: vec
      type(list), pointer :: nl
      integer :: N, i
      N = size(vec);
      if(N==0) then
         nullify(nl)
      else
         nl => new_list(vec(1))
         do i=2,N
            nl => cons(vec(i), nl)
         end do
      end if
    end function new_list_from_vector

end module LinkedList
