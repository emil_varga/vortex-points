module Units
  real, parameter :: L = 1 !cm
  real, parameter :: kappa = 1e-6 !cm^2/s
  real, parameter :: tau   = L**2/kappa !s
end module Units
