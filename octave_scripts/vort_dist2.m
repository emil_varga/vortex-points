function vort_dist2(pat)
  if(nargin<1) pat = "test*.dat"; endif
  files = dir(pat);
  N = length(files);
  res = []; Ts = [];
  for i=1:N
    data = load(files(i).name);
    time = data(1,2);
    x1 = data(1,4); y1 = data(1,5);
    vx1 = data(1,6); vy1 = data(1,7);
    x2 = data(2,4); y2 = data(2,5);
    vx2 = data(2,6); vy2 = data(2,7);

    dx = x1 - x2;
    dy = y1 - y2;

    r = sqrt(dx^2 + dy^2);
    Ts = [Ts; time];
    res = [res; r, x1, y1, vx1, vy1, x2, y2, vx2, vy2];
  end

  plot(Ts, res(:,1),'-o'); legend('relative distance'); print -deps -color 'rel_dist.eps';
  figure; hold on; plot(Ts, res(:,2), '-ob'); plot(Ts, res(:,6), '-or'); hold off; legend('x-coordinate of the 1st vortex', '2nd');
  print -deps -color 'xpos.eps';
  figure; hold on; plot(Ts, res(:,4), '-ob'); plot(Ts, res(:,8), '-or'); hold off; legend('vx of the 1st vortex', '2nd');
  print -deps -color 'vx.eps'
endfunction
