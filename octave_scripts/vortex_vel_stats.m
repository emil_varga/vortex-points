function vortex_vel_stats(l=0.5, u=inf, pat='2-test*.dat')
%function vortex_vel_stats(l=0.5, u=inf, pat='2-test*.dat')
  files = dir(pat);
  N = length(files);
  
  xvels = [];
  yvels = [];
  signs = [];

  for i=1:N
    data = load(files(i).name);
    xvels = [xvels; data(:,6)];
    yvels = [yvels; data(:,7)];
    signs = [signs; data(:,3)];
  endfor

  vels = sqrt(xvels.^2 + yvels.^2);

  xvels(signs>0) = xvels(signs>0) - mean(xvels(signs>0));
  xvels(signs<0) = xvels(signs<0) - mean(xvels(signs<0));

  yvels = yvels - mean(yvels);
  rawseries = [xvels, yvels, vels];
  save('-ascii', 'vort_rawseries_v.dat', 'rawseries');
  
  std_vx = std(xvels);
  std_vy = std(yvels);
  std_v  = std(vels);
  mean_vy = mean(yvels);

  Nvort = size(data,1);
  Nbins = Nvort;
  binsp = linspace(-20*std_vx, 20*std_vx, Nbins);
  [nnx, xx] = hist(xvels, binsp, 1);
  binsp = linspace(-20*std_vy, 20*std_vy, Nbins);
  [nny, yy] = hist(yvels, binsp, 1);
  binsp = linspace(-20*std_v, 20*std_v, Nbins);
  [nnv, vv] = hist(vels, binsp, 1);
  res = [xx'/std_vx, nnx', yy'/std_vy, nny'];
  save('-ascii', 'vortex-vel-stats-all.dat', 'res');

  xs = log(abs(yy));
  ys = log(nny);
  [xs, is] = sort(xs);
  ys = ys(is);
  idx = (xs>l) & (xs<u) & (~isnan(ys)) & (~isinf(ys));

  xsx = log(abs(xx));
  ysx = log(nnx);
  [xsx, is] = sort(xsx);
  ysx = ysx(is);
  
  p = polyfit(xs(idx), ys(idx), 1);
  a0 = mean(ys(idx) + 3*xs(idx));
  plot(xs, ys, xsx, ysx, xs, p(2) + xs*p(1), 'r', xs, a0 - 3*xs, 'k') 

  xlabel('log(v/v_{sd})');
  ylabel('log(p.d.f)');
  legend({"v_y - mean(v_y)",...
	  "v_x",...
	  [num2str(p(1)), " power law"],...
	  "-3 power law"});
  legend("location", "southwest");
  title(['velocity statistics of ', num2str(Nvort), ' vortices']);
  filename = [num2str(Nvort) 'vort-vort-vel-pdf.eps'];
  print('-deps', '-color', filename);
endfunction
