function radial_vortex_distribution(filepat, r_bins = 20, r_max = 1/sqrt(2))

  files = dir(filepat);
  N = length(files);

  rs = linspace(0, r_max, r_bins);
  dr = r_max / r_bins;
  bin_centers = rs + 0.5*dr;

  data = load(files(1).name);
  Nvort = length(data(:,1));

  for i=1:N
    l = length(files(i).name);
    filename = [(files(i).name)(1:(l - 4)), '_rvt.jpg'];
    if(!exist(filename, "file"))
      data = load(files(i).name);
      t = data(1, 2);
      sgn = data(:,3);
      rp = sqrt((data(sgn>0, 4)-0.5).^2 + (data(sgn>0, 5)-0.5).^2);
      rn = sqrt((data(sgn<0, 4)-0.5).^2 + (data(sgn<0, 5)-0.5).^2);
      [nsp, bc] = hist(rp, bin_centers);
      nsp = nsp./([pi*dr^2, 2*pi*rp'(2:r_bins)*dr]);

      [nsn, bc] = hist(rn, bin_centers);
      nsn = nsn./([pi*dr^2, 2*pi*rn'(2:r_bins)*dr]);
      
      plot(bin_centers, nsp, 'bo-',...
	   bin_centers, nsn, 'rx-',...
	   bin_centers, nsp+nsn, 'k*-');
      title(['time: ', sprintf("%.4f", t)]);
      axis([0, r_max + dr, 0, 10*Nvort]); 
      print(filename, '-djpg');
    endif
  endfor
  close all;
endfunction
