function plot_trajectory(k, pat)
  if(nargin < 2)
    pat = 'test*.dat';
  endif

  files = dir(pat);
  N = length(files);

  tr = zeros(N,3);
  for i=1:N
    data = load(files(i).name);
    tr(i,:) = [data(1,2), data(k,4:5)];
  end

  plot(tr(:,2), tr(:,3), 'o');
end