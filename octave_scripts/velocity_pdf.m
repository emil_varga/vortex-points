function data_all = velocity_pdf(Nvort, filenames, mindist = 0, l=0.2, u=inf,...
				 outname = 'vel-hist.dat', data_all = [], D=0, fig=-1)
%data_all = velocity_pdf(Nvort, filenames, mindist = 0, l=0.2, u=inf,
%				 outname = 'vel-hist.dat', data_all = [], D=0, fig=-1)

  if(isempty(data_all))
    files = dir(filenames);
    N = length(files);
    for i=1:N
      data = load(files(i).name);
      data_all = [data_all; data(:,6:8)];
    endfor
  endif
  if(length(mindist)==1)
    mean_dist = mean(data_all(:,3))
    std_dist  = std(data_all(:,3))
    min_dist  = min(data_all(:,3))
    max_dist  = max(data_all(:,3))
  endif
  for k=1:length(mindist)
    idx = (data_all(:,3) >= mindist(k));
    vx = data_all(idx,1);
    vy = data_all(idx,2);
  

    mean_vy = mean(vy);
    vy = vy - mean_vy;

    std_vx = std(vx)
    std_vy = std(vy)
    
    cutoff = 20*max([std_vx, std_vy])

    idx = abs(vx)<cutoff & abs(vy)<cutoff;
    vx = vx(idx);
    vy = vy(idx);
    v = sqrt(vx.^2 + vy.^2);
    raw = [vx, vy, v];
    save('-ascii', 'grid_rawseries_v.dat', 'raw');
    clear raw, v;

    std_vx = std(vx)
    std_vy = std(vy)

    [nnx, xx] = hist(vx/std_vx, 200, 1);
    [nny, yy] = hist(vy/std_vy, 200, 1);
    semilogy(xx, nnx);
    
    res = [xx', nnx', yy', nny'];
    
    save([num2str(D), outname], 'res', '-ascii');
    D = D+1
  endfor
endfunction
