function data = amplitude(data_file, cutoff=inf, scale=1)
%function data = amplitude(data_file, cutoff=inf, scale=1)
  data = [];

  if(ischar(data_file))
    data = load(data_file);
  elseif(ismatrix(data_file))
    data = data_file;
  endif

  vector = data(:,3);
  vector = vector(abs(vector) < cutoff);
  
  amplitude = mean(vector)*scale
endfunction
