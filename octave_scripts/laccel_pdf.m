function data_all = laccel_pdf(Nvort, filenames, mindist = 0, l=0.2, u=inf,\
			       outname = 'laccel-hist.dat', data_all = [], D=0, fig=-1)
%data_all = laccel_pdf(Nvort, filenames, mindist = 0, l=0.2, u=inf,
%			       outname = 'laccel-hist.dat', data_all = [], D=0, fig=-1)
  if(isempty(data_all))
    files = dir(filenames);
    N = length(files);
    for i=1:N
      data = load(files(i).name);
      data_all = [data_all; data(:,6:8)];
    endfor
    cutoff = 1e6;
    idx = (sqrt(data_all(:,1).^2 + data_all(:,2).^2) < cutoff);
    data_all = data_all(idx, :);
  endif

  idx = (data_all(:,3) >= mindist(1));
  ax = data_all(idx,1);
  ay = data_all(idx,2);
  
  mean_ay = mean(ay);
  ay = ay - mean_ay;

  a = sqrt(ax.^2 + ay.^2);
  raw = [ax, ay, a];
  save('-ascii', 'grid_rawseries_laccel.dat', 'raw')
  clear raw, a;

  std_ax = std(ax)
  std_ay = std(ay)

  [nnx, xx] = hist(ax/std_ax, 200, 1);
  [nny, yy] = hist(ay/std_ay, 200, 1);
  if(fig == -1) fig = figure; endif
  figure(fig)
  hold on;
  hold all;
  semilogy(xx, nnx);
  hold off;

  res = [xx', nnx', yy', nny'];

  lxx = log(abs(xx));
  lnnx = log(nnx);
  lyy = log(abs(yy));
  lnny = log(nny);

  [lxx, ix] = sort(lxx);
  lnnx = lnnx(ix);
  [lyy, iy] = sort(lyy);
  lnny = lnny(iy);

  iy = (lyy > l) & (lyy < u) & (~isnan(lnny)) & (~isinf(lnny));
  ix = (lxx > l) & (lxx < u) & (~isnan(lnnx)) & (~isinf(lnnx));
  
  psx = polyfit(lxx(ix), lnnx(ix), 1);
  psy = polyfit(lyy(iy), lnny(iy), 1);
  
  a = mean(lnnx(ix) + 1.66*lxx(ix));
  figure; plot(lxx, lnnx, lxx, psx(2) + lxx*psx(1), 'r', lxx, a  -1.66*lxx, 'k');
  legend('a_x', [num2str(psx(1)), ' power law']...
	,'-1.66 power law');
  xlabel('log(a/a_{sd})');
  ylabel('log(p.d.f.)');
  title(['statistics of acceleration component on a grid, ', num2str(Nvort), ' vortices']);
  figfilename = [num2str(Nvort) 'vort-grid-vel-pdf.eps'];
  print('-deps', '-color', figfilename);
  save([num2str(D), outname], 'res', '-ascii');
  if(length(mindist) > 1)
    laccel_pdf(Nvort, [], mindist(2:length(mindist)), l, u, outname, data_all, D+1, fig);
  endif
endfunction
