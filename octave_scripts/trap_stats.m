function trap_stats(pat = 'part-*.dat')
  files = dir(pat);
  N = length(files);
  data = load(files(1).name);
  P = length(data(:,1));

  trapped = zeros(P,1);
  freetime = zeros(P,1);
  trappedtime = zeros(P,1);
  trap_events = zeros(P,1);
  untrap_events = zeros(P,1);
  time = 0;
  prevtime = 0;

  ts = [];
  Ntrap = [];
  Nfree = [];
  vm = [];
  vm_free = [];
  vm_trap = [];

  for i=1:N
    data = load(files(i).name);
    prevtime = time;
    time = data(1,2);
    freetime(trapped == 0) = freetime(trapped == 0) + time-prevtime;
    trappedtime(trapped > 0) = trappedtime(trapped > 0) + time-prevtime;

    curtrap = data(:,9) > 0;
    trapidx = ((trapped - curtrap) < 0);
    untrapidx = ((curtrap - trapped) < 0);

    trap_events(trapidx) = trap_events(trapidx) + 1;
    untrap_events(untrapidx) = untrap_events(untrapidx) + 1;

    trapped = curtrap;
    free_p = ~trapped;

    ts = [ts; time];
    trp = sum(trapped > 0);
    Ntrap = [Ntrap; trp];
    Nfree = [Nfree; P - trp];
    vx = data(:,5); vy = data(:,6);
    vs = sqrt(vx.^2 + vy.^2);
    
    vm = [vm; mean(vs)];
    vm_free = [vm_free; mean(vs(free_p))];
    vm_trap = [vm_trap; mean(vs(trapped))];
  end
  figure; plot(ts, Ntrap, ts, Nfree);
  figure; plot(ts, vm, ts, vm_free, ts, vm_trap);
  r = [ts, Ntrap, Nfree];
  save('-ascii', 'ntrap_nfree_t.txt', 'r');
  r = [ts, vm, vm_free, vm_trap];
  save('-ascii', 'vm_free_trap_t.txt', 'r');

  mean_free_time = mean(freetime./(trap_events + 1))
  std_mean_free_time = std(freetime./(trap_events + 1))
  mean_trapped_time = mean(trappedtime./(untrap_events + 1))
  std_mean_trapped_time = std(trappedtime./(untrap_events + 1))
endfunction
