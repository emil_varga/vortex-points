function filtered_free_part(min_dist = 0, pat = 'part-*.dat')
%function filtered_free_part(min_dist = 0, pat = 'part-*.dat')
  vortices = load('1-test00000.dat');

  files = dir(pat);
  N = length(files);

  %free
  fvxs = []; fvys = [];
  faxs = []; fays = [];

  for i=1:N
    data = load(files(i).name);
    filt_idx = (data(:,9) == 0) & (data(:,10) >= min_dist);

    fvxs = [fvxs; data(filt_idx,5)];
    fvys = [fvys; data(filt_idx,6)];
    faxs = [faxs; data(filt_idx,7)];
    fays = [fays; data(filt_idx,8)];
  end

  Nbins  = 400;

  mean_fvys = mean(fvys)
  mean_fvxs = mean(fvxs)

  fvxs = fvxs(abs(fvxs)<100);
  fvys = fvys - mean_fvys;
  fvxs = fvxs - mean_fvxs;
  std_fvx = std(fvxs)
  std_fvy = std(fvys)

  [fnnvx, fvx] = hist(fvxs, Nbins, 1);

  [fnnvy, fvy] = hist(fvys, Nbins, 1);

  lfvx = log(abs(fvx)/std_fvx);
  [lfvx, is] = sort(lfvx);
  lfnnvx = log(fnnvx(is));

  idx = lfvx > 0 & ~isnan(lfvx) & ~isnan(lfnnvx) & ~isinf(lfvx) & ~isinf(lfnnvx);
  p_free = polyfit(lfvx(idx), lfnnvx(idx), 1);

%  plot(lfvx, lfnnvx, 'linewidth', 1, 'linestyle', '--',...
%              lfvx, p_free(1)*lfvx + p_free(2), 'linewidth', 2.5);
%  legend({''; ['power law', num2str(p_free(1))]});
%  title('statistics of x component of tracer particle velocity');
%  xlabel('log(v/v_{sd})');
%  ylabel('log(p.d.f.)');

  faxs = faxs(~isnan(faxs));
  fays = fays(~isnan(fays));

  fays = fays(abs(fays)<1e4);
  faxs = faxs(abs(faxs)<1e4);


  mean_faxs = mean(faxs)
  mean_fays = mean(fays)
  std_fax = std(faxs)
  std_fay = std(fays)
  [fnnay, fay] = hist(fays, Nbins, 1);
  [fnnax, fax] = hist(faxs, Nbins, 1);
  lfax = log(abs(fax)/std_fax);
  [lfax, is] = sort(lfax);
  lfnnax = log(fnnax(is));

  idx = lfax > -2 & ~isnan(lfax) & ~isnan(lfnnax) & ~isinf(lfax) & ~isinf(lfnnax);
  p_free = polyfit(lfax(idx), lfnnax(idx), 1);

  plot(lfax, lfnnax, 'linewidth', 1, 'linestyle', '--',...
       lfax, lfax*p_free(1) + p_free(2));
  legend({''; ['power law', num2str(p_free(1))]});
%  semilogy(fax/std_fax, fnnax, fay/std_fay, fnnay); legend({'x-component', 'y-component'});
%  title('statistics of y-component of tracer particle acceleration');
  xlabel('log(a/a_{sd})');
  ylabel('p.d.f.');
endfunction
