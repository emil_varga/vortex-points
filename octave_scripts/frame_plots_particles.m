function frame_plots(pat)
  if(nargin < 1)
    pat = 'part*.dat';
  endif

  files = dir(pat);
  
  N = length(files);
  
  for i=1:N
    data = load(files(i).name);
    if(nargin > 1)
      pdata = load(pfiles(i).name);
      ppos = pdata(:,3:4);
    endif
    time = data(1,2);
    xs = data(:,3);
    ys = data(:,4);
    
    plot(xs, ys, 'g*');
    title(['time: ', num2str(time)]);
    axis([0, 1, 0, 1], 'equal')
    l = length(files(i).name);
    filename = [(files(i).name)(1:(l-4)), '.jpg'];
    print(filename, '-djpg');
  endfor
  close all;
endfunction

