1;

function save_stats(fid, data, c)
  xm = mean(data(:,1));
  xms = std(data(:,1));

  ym = mean(data(:,2));
  yms = std(data(:,2));
  
  mm = mean(data(:,3));
  mms = std(data(:,3));

  fprintf(fid, ['\t', c, '_abs=\t%e \t %e\n',...
		 '\tmean ', c, '_x=\t%e \t %e\n',...
		 '\tmean ', c, '_y=\t %e \t %e\n'],...
	  mm, mms, xm, xms, ym, yms);
endfunction


file = fopen('analysis.txt', 'w');

printf('basic statistics\n');
%%%% particles

fprintf(file, 'PARTICLES:\n');
%%all
printf('all particles\n');
fprintf(file, 'all particles\n');
data = load('part_rawseries_all_v.dat');
save_stats(file, data, 'v');
data = load('part_rawseries_all_a.dat');
save_stats(file, data, 'A');

%%free
printf('free particles\n');
fprintf(file, 'free particles\n');
data = load('part_rawseries_free_v.dat');
save_stats(file, data, 'v');
data = load('part_rawseries_free_a.dat');
save_stats(file, data, 'A');

%%trapped
printf('trapped particles\n');
fprintf(file, 'trapped particles\n');
data = load('part_rawseries_trap_v.dat');
save_stats(file, data, 'v');
data = load('part_rawseries_trap_a.dat');
save_stats(file, data, 'A');

%%%% vortices
fprintf(file, 'VORTICES:\n');
printf('vortices\n');
data = load('vort_rawseries_v.dat');
save_stats(file, data, 'v');
data = load('vort_rawseries_a.dat');
save_stats(file, data, 'A');

%%%% grid
fprintf(file, 'GRID POINTS:\n');
printf('grid points\n');
data = load('grid_rawseries_v.dat');
save_stats(file, data, 'v');
data = load('grid_rawseries_laccel.dat');

fprintf(file, 'grid laccels without cutoff\n');
save_stats(file, data, 'A');
fprintf(file, 'grid laccels with 1e5 cutoff\n');
idx = (data(:,3) < 1e5);
save_stats(file, data(idx, :), 'A');
fclose(file);
