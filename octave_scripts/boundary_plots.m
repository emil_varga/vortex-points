function boundary_plots(filename)
  data = load(filename);

  points = data(:,1);
  zero = zeros(size(points));
  one  = ones(size(points));

  vwx = data(:,2); vwy = data(:,3);
  vsx = data(:,4); vsy = data(:,5);
  vex = data(:,6); vey = data(:,7);
  vnx = data(:,8); vny = data(:,9);

  figure;
  plot(vwx, points, points, vsx, 1+vex, points, points, 1+vnx); axis('square');
  print -deps -color 'vbx.eps'
  figure;
  plot(vwy, points, points, vsy, 1+vey, points, points, 1+vny); axis('square');
  print -deps -color 'vbx.eps'
endfunction
