function accel_PDF(Nvort, l=0.2, u=inf)
  data1 = load('velocity-stats-mid-00000.dat');
  data2 = load('velocity-stats-mid-00001.dat');
  data3 = load('velocity-stats-mid-00002.dat');

  t1 = data1(1,2); t2 = data2(1,2); t3 = data3(1,2);

  vx1 = data1(:,6); vy1 = data1(:,7);
  vx2 = data2(:,6); vy2 = data2(:,7);
  vx3 = data3(:,6); vy3 = data3(:,7);

  dt1 = t2 - t1;
  dt2 = t3 - t2;

  ax = (vx3*dt1/dt2 - vx1*dt2/dt1 - (dt1/dt2 - dt2/dt1)*vx2)/(dt1+dt2);
  ay = (vy3*dt1/dt2 - vy1*dt2/dt1 - (dt1/dt2 - dt2/dt1)*vy2)/(dt1+dt2);

  max(ax)
  max(ay)
  std_ax = std(ax)
  std_ay = std(ay)

  cutoff = max([std_ax, std_ay])

  ax = ax(abs(ax)<cutoff);
  ay = ay(abs(ay)<cutoff);

  [nnx, xx] = hist(ax/std_ax, 200, 1);
  [nny, yy] = hist(ay/std_ay, 200, 1);


  res = [xx', nnx', yy', nny'];

  lxx = log(abs(xx));
  lnnx = log(nnx);
  lyy = log(abs(yy));
  lnny = log(nny);

  [lxx, ix] = sort(lxx);
  lnnx = lnnx(ix);
  [lyy, iy] = sort(lyy);
  lnny = lnny(iy);

  iy = (lyy > l) & (lyy < u) & (~isnan(lnny)) & (~isinf(lnny));
  ix = (lxx > l) & (lxx < u) & (~isnan(lnnx)) & (~isinf(lnnx));
  
  psx = polyfit(lxx(ix), lnnx(ix), 1);
  psy = polyfit(lyy(iy), lnny(iy), 1);

  plot(lxx, lnnx, lyy, lnny, lxx, psx(2) + lxx*psx(1), lyy, psy(2) + lyy*psy(1));
  legend('a_x', 'a_y', [num2str(psx(1)), ' power law'], [num2str(psy(1)), ' power law']);
  xlabel('log(a/a_{sd})');
  ylabel('log(p.d.f.(a/a_{sd}))');
  title(['statistics of acceleration component on a grid, ', num2str(Nvort), ' vortices']);
  figfilename = [num2str(Nvort) 'vort-grid-accel-pdf.eps'];
  print('-deps', '-color', figfilename);

  save('-ascii', 'accel_PDF.dat', 'res')
endfunction
