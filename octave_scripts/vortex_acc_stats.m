function vortex_acc_stats(l=0, u=inf, pat='test', five=false)
%function vortex_acc_stats(l=0, u=inf, pat='test', five=false)
  files1 = dir(['1-', pat, '*.dat']);
  files2 = dir(['2-', pat, '*.dat']);
  files3 = dir(['3-', pat, '*.dat']);
  if(nargin > 3)
    files4 = dir(['4-', pat, '*.dat']);
    files5 = dir(['5-', pat, '*.dat']);
  end

  dtsdata = load('timesteps.dat');
  timesteps = dtsdata(:,2);
  time      = cumsum(timesteps);
  mdt = mean(timesteps);
  K = length(timesteps);

  N = length(files1);
  ax = []; ay = [];
  for i=1:N
    data1 = load(files1(i).name);
    data2 = load(files2(i).name);
    data3 = load(files3(i).name);
    if(five)
      data4 = load(files4(i).name);
      data5 = load(files5(i).name);
    end

    vx1 = data1(:,6); vy1 = data1(:,7); t1 = data1(1,2);
    vx2 = data2(:,6); vy2 = data2(:,7); t2 = data2(1,2);
    vx3 = data3(:,6); vy3 = data3(:,7); t3 = data3(1,2);
    if(five)
      vx4 = data4(:,6); vy4 = data4(:,7); t4 = data4(1,2);
      vx5 = data5(:,6); vy5 = data5(:,7); t5 = data5(1,2);
    end
    
    if(~five)
      [tt, idx] = min(abs(time - t2));
      if(idx < K)
	dt1 = timesteps(idx);
	dt2 = timesteps(idx+1);
	%dt1 = t2-t1;
	%dt2 = t3-t2;
	
	ax = [ax; (dt1/dt2*vx3 - dt2/dt1*vx1 - (dt1/dt2 - dt2/dt1)*vx2)/(dt1 + dt2)];
	ay = [ay; (dt1/dt2*vy3 - dt2/dt1*vy1 - (dt1/dt2 - dt2/dt1)*vy2)/(dt1 + dt2)];

	if(length(ax) != length(ay))
	  error(['unequal acceleration vectors ', files1(i).name,' ', files2(i).name,' ', files3(i).name]);
	endif
      end
    else
      [tt, idx] = min(abs(time-t3));
      if(idx < K-1 && idx > 1)
	h1 = timesteps(idx-1)*1e6;
	h2 = timesteps(idx)*1e6;
	h4 = timesteps(idx+1)*1e6;
	h5 = timesteps(idx+2)*1e6;
	h1 = h1+h2;
	h1 = -h1;
	h2 = -h2;
	h5 = h4+h5;
	
	g1 = -h2*h4*h5/h1/(h1-h2)/(h1-h4)/(h1-h5);
	g2 = -h1*h4*h5/h2/(h2-h1)/(h2-h4)/(h2-h5);
	g3 = -(1/h1 + 1/h2 + 1/h4 + 1/h5);
	g4 = -h1*h2*h5/h4/(h4-h1)/(h4-h2)/(h4-h5);
	g5 = -h1*h4*h2/h5/(h5-h1)/(h5-h2)/(h5-h4);

	ax = [ax; (g1*vx1 + g2*vx2 + g3*vx4 + g4*vx4 + g5*vx5)*1e6];
	ay = [ay; (g1*vy1 + g2*vy2 + g3*vy4 + g4*vy4 + g5*vy5)*1e6];

      end
    end
  endfor


   
  idx = abs(ax)<1e4 & abs(ay)<1e4;
  ax = ax(idx);
  ay = ay(idx);
  a = sqrt(ax.^2 + ay.^2)

  raw_series = [ax, ay, a];
  save('-ascii', 'vort_rawseries_a.dat', 'raw_series');

  std_ax = std(ax)
  std_ay = std(ay)
  
  [nnx, xx] = hist(ax, 250, 1);
  [nny, yy] = hist(ay, 250, 1);
  [nna, aa] = hist(a, 250, 1); 

  hist_component = [xx'/std_ax, nnx', yy'/std_ay, nny'];
  hist_mag = [aa', nna']
  save('-ascii', 'vort_hist_a_component.dat', 'hist_component');
  save('-ascii', 'vort_hist_a_magnitude.dat', 'hist_mag');

  Nvort = size(data1)(1);

  xs = log(abs(yy));
  ys = log(nny);
  [xs, is] = sort(xs);
  ys = ys(is);
  idx = (xs>l) & (xs<u) & (~isnan(ys)) & (~isinf(ys));

  xxs = log(abs(xx));
  xys = log(nnx);
  [xxs, is] = sort(xxs);
  xys = xys(is);
  
  a0 = mean(ys(idx) + 1.66*xs(idx));
  p = polyfit(xs(idx), ys(idx), 1);
  plot(xs, ys, xxs, xys, xs, p(2) + xs*p(1), 'r', xs, a0 - 1.66*xs, 'k'); 
  axis([-3,max(xs), min(ys), max(ys)]);
  xlabel('log(a/a_{sd})');
  ylabel('log(p.d.f.)');
  legend('a_x', 'a_y', [num2str(p(1)), ' power law'], '-1.66 power law');
  legend("location", "southwest");
  title(['acceleration statistics of ', num2str(Nvort), ' vortices']);
  filename = [num2str(Nvort) 'vort-vort-accel-pdf.eps'];
  print('-deps', '-color', filename);
endfunction
