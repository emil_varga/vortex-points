function plot_particle_trajectory(k, tstart, tend, pat)
  if(nargin < 2)
    pat = 'part-*.dat';
    tstart = 0;
    tend = inf;
  endif
  if(nargin < 4)
    pat = 'part-*.dat';
  endif


  files = dir(pat);
  N = length(files);

  tr = [];
  for i=1:N
    data = load(files(i).name);
    if(data(1,2) > tstart && data(1,2) < tend)
      tr = [tr; [data(1,2), data(k,3:4)]];
    elseif(data(1,2) > tend)
      break;
    endif
  end
  plot(tr(:,2), tr(:,3), '-o');
  axis([0, 1, 0, 1], 'equal');
end
