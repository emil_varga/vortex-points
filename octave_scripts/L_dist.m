function L_dist(filepat, bins=10)

  files = dir(filepat);
  Nf = length(files);

  for i=1:Nf
    rfilename = ['Ldist-', files(i).name];
    ix = strfind(rfilename, 'dat');
    img_filename = rfilename;
    img_filename(ix:(ix+2)) = 'jpg';
    if(!exist(img_filename, "file"))
      xs = linspace(0, 1, bins+1);
      Lxn = Lxp = zeros(1, bins);
      Lyn = Lyp = zeros(1, bins);

      data = load(files(i).name);
      vxs = data(:,4); vys = data(:,5);
      circs = data(:,3);
      N = length(vxs);
      
      for k=1:N
	if(circs(k) > 0)
	  Lxp(search_idx(xs, vxs(k)))++;
	  Lyp(search_idx(xs, vys(k)))++;
	else
	  Lxn(search_idx(xs, vxs(k)))++;
	  Lyn(search_idx(xs, vys(k)))++;
	endif
      endfor
      xs = (xs+0.5*mean(diff(xs)))(1:bins);
      Lxp/=0.5*N; Lxn/=0.5*N; Lyp/=0.5*N; Lyn/=0.5*N;
      r = [xs', Lxp', Lxn', Lyp', Lyn'];
      save('-ascii', rfilename, 'r');

      subplot(3,1,1);
      plot(xs, 0.5*(Lxp + Lxn), 'k');
      axis([0,1,0,1]);
      subplot(3,1,2);
      plot(xs, Lxp, 'r', xs, Lxn, 'b');
      axis([0,1,0,1]);
      subplot(3,2,5);
      plot(0.5*(Lyp+Lyn), xs, 'k');
      axis([0,0.5,0,1]);
      subplot(3,2,6);
      plot(Lyp, xs, 'r', Lyn, xs, 'b');
      axis([0,0.5,0,1]);

      print(img_filename, '-djpg');
    endif
  endfor
endfunction

function idx = search_idx(xs, x)
  l = 1;
  u = length(xs);
  while(u-l > 1)
       mid = floor(mean([l,u]));
       if(x < xs(mid)) 
	 u = mid; 
       else
	 l = mid;
       endif
  endwhile
  idx = l;
endfunction
