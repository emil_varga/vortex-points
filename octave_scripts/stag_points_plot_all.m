function stag_points_plot_all(pat)
  if(nargin < 1)
    pat = 'test*.dat';
  endif

  files = dir(pat);
  N = length(files);
  for i=1:N
    [sxs, sys] = stagnation_points(files(i).name, 10, 'min');
    [sxsm, sysm] = stagnation_points(files(i).name, 10, 'max');
    l = length(files(i).name);
    filename = ['stagp-', (files(i).name)(1:(l-4)), '.jpg'];
    plot(sxs, sys, 'bo', sxsm, sysm, 'rx');
    print(filename, '-djpg');
    printf('%d/%d\n', i, N);
  end
endfunction
