function vs_dist_plots(filepat, vsrng, vnrng, vnext, ampl, n)
  %function vs_dist_plots(filepat, vsrng, vnrng, vnext, ampl, n)
  %filepat = glob file pattern
  %vsrng, vnrng = v_s and v_n ranges, rank(1,2) vectors
  %vnext = flat external v_n, the y direction
  %ampl = amplitude of the cosine v_n profile
  %n    = number of periods for the cosine v_n profile
  files = dir(filepat);
  Nf = length(files);

  for i=1:Nf
    ix = strfind(files(i).name, 'dat');
    img_filename = files(i).name;
    img_filename(ix:(2+ix)) = 'jpg';
    if(!exist(img_filename, "file"))
      data = load(files(i).name);

      subplot(2,1,1);
      plot(data(:,1), data(:,2), data(:,1), ones(size(data(:,2)))*mean(data(:,2)), 'r');
      ylabel('v_s')
      axis([0,1,vsrng]);

      subplot(2,1,2);
      plot(data(:,1), vnext + ampl*cos(2*pi*n*data(:,1)) - data(:,2));
      ylabel('v_n - v_s');
      xlabel('x');
      axis([0,1,vnrng]);

      print(img_filename, '-djpg');
    endif
  endfor

endfunction
