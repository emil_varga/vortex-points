function plot_energy(filename)
  if(nargin < 1)
    filename = 'energy.dat';
  endif

  data = load('energy.dat');
  E100 = data(:,2);
  E500 = data(:,3);
  E1000 = data(:,4);
  t = data(:,1);

  plot(t, E100, '-ob', t, E500, '-xr', t, E1000, '-*g');
  legend('E100', 'E500', 'E1000');
  print -deps -color 'energy.eps'
endfunction
