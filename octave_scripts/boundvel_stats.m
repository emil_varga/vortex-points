function boundvel_stats(filename)
  data = load(filename);
  
  %west-east
  wediffx = 2*abs((data(:,2) - data(:,6))./(abs(data(:,2)) + abs(data(:,6))))*100;
  wediffy = 2*abs((data(:,3) - data(:,7))./(abs(data(:,3)) + abs(data(:,7))))*100;

  %north-south
  nsdiffx = 2*abs((data(:,8) - data(:,4))./(abs(data(:,8)) + abs(data(:,4))))*100;
  nsdiffy = 2*abs((data(:,9) - data(:,5))./(abs(data(:,9)) + abs(data(:,5))))*100;

  printf("Max west-east difference: vx %f%%, vy %f%%; mean: vx (%f +/- %f)%%, vy (%f +/- %f)%%\n",...
	 max(wediffx), max(wediffy), mean(wediffx), std(wediffx), mean(wediffy), std(wediffy));

  printf("Max north-south difference: vx %f%%, vy %f%%; mean: vx (%f +/- %f)%%, vy (%f +/- %f)%%\n",...
	 max(nsdiffx), max(nsdiffy), mean(nsdiffx), std(nsdiffx), mean(nsdiffy), std(nsdiffy));
endfunction
