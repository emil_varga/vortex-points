function average_sim_decays(filepat, fileout='avg_sim_decay.txt')
%function average_sim_decays(filepat, fileout='avg_sim_decay.txt')
%filepat = files with timesteps
  files = dir(filepat);
  N = length(files);
  max_t = 0; NTs = 0;
  mean_dt = 0;
  for i = 1:N
    data = load(files(i).name);
    dts = data(:,2); m = length(dts);

    mean_dt = mean_dt + mean(dts)*m;
    NTs = NTs + m;
    end_t = sum(dts);
    if(end_t > max_t) 
      max_t = end_t;
    endif
  endfor
  mean_dt = mean_dt / NTs;

  time_base = (0:mean_dt:max_t)';
  avgNvort = zeros(size(time_base));

  for i=1:N
      data = load(files(i).name);
      dts = data(:,2); Nvort = data(:,3);
      t = cumsum(dts);
      %extrapolating with zero. This is correct since the program ends when N=0
      avgNvort = avgNvort + interp1(t, Nvort, time_base, 0);
  endfor
  avgNvort = avgNvort/N;

  d = [time_base, avgNvort];
  save('-ascii', fileout, 'd');
  
endfunction
