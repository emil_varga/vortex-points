function frame_plots(pat, side=1, particles)
  if(nargin < 1)
    pat = 'test*.dat';
  endif

  files = dir(pat);
  if(nargin > 2)
    pfiles = dir(particles);
  endif

  
  N = length(files);
  
  for i=1:N
    l = length(files(i).name);
    filename = [(files(i).name)(1:(l-4)), '.jpg'];
    if(!exist(filename, "file"))
      data = load(files(i).name);
      if(nargin > 2)
	pdata = load(pfiles(i).name);
	ppos = pdata(:,3:4);
      endif
      up   = data(:,3) > 0;
      down = data(:,3) < 0;
      time = data(1,2);
      xs = data(:,4);
      ys = data(:,5);
      
      if(nargin > 2)
	plot(xs(up), ys(up), 'bo', 'markerfacecolor','b'...
	     ,xs(down), ys(down), 'rx', 'markerfacecolor', 'r'...
	     ,ppos(:,1), ppos(:,2), 'g*');
      else
	plot(xs(up), ys(up), 'bo', 'markerfacecolor','b'...
	     ,xs(down), ys(down), 'ro', 'markerfacecolor', 'r');
      endif
      title(['time: ', sprintf("%.4f", time)]);
      axis([0.5 - side/2, 0.5 + side/2, 0.5 - side/2, 0.5 + side/2], 'equal')
      print(filename, '-djpg');
    endif
  endfor
  close all;
endfunction

