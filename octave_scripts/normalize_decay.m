function normalize_decay(filename, N0, fileout=['N',num2str(N0), 'normdec-', filename])
%function normalize_decay(filename, N0, fileout=['N',num2str(N0), 'dec-', filename])
%filename = file with timesteps, N0 = starting number of vortices
  data = load(filename);

  %normalize vortex line density
  Nvort = data(:,2);
  Nvort = Nvort / N0;

  %get time
  timesteps = data(:,2);
  time = cumsum(timesteps);

  %normalize time with time when N reaches 0.1% of N0
  idx = Nvort < 1e-3;
  T0 = time(idx)(1);
  time = time/T0;

  data = [time, Nvort];
  save('-ascii', fileout, 'data');
endfunction
