function velocity_pdf_coarse_aa(Nbins, filenames, nns,...
			        flatness_outname = 'vpdf_flatness.txt',...
				outname = 'N-vel-hist-coarse.dat',...
				outname_raw = 'N-vel-rawseries-coarse.dat')
%function velocity_pdf_coarse_aa(Nbins, filenames, sideboxes,...
%			        flatness_outname = 'vpdf_flatness.txt',...
%				outname = 'N-vel-hist-coarse.dat',...
%				outname_raw = 'N-vel-rawseries-coarse.dat')
  files = dir(filenames);
  Nf = length(files);
  vpdf_flatness = [];
  
  for i=1:Nf
    printf(["Loading ", files(i).name, "... "]); fflush(stdout);
    tmp(:,:) = load(files(i).name);
    data_files(i,:,:) = tmp(:,4:7);
    printf('done.\n'); fflush(stdout);
  endfor
  clear tmp;
  Ns = length(nns);
  for S=1:Ns
    data_all = [];
    for i = 1:Nf
      printf("Coarse graining %s with %d n.n.s... ", files(i).name, nns(S)); fflush(stdout);
      tmp(:,:) = data_files(i,:,:);
      [cvxs, cvys] = nn_coarse_grain(tmp, nns(S));
      printf('done.\n'); fflush(stdout);
      data_all = [data_all; cvxs, cvys];
    endfor
    clear tmp

    mean_vy = mean(data_all(:,2))
    data_all(:,2) -= mean_vy;

    std_vx = std(data_all(:,1))
    std_vy = std(data_all(:,2))

    cutoff = 20*max([std_vx, std_vy])
    v = sqrt(data_all(:,1).^2 + data_all(:,2).^2);
    idx = abs(v) < cutoff;

    [nnvx, vx] = hist(data_all(idx,1)/std_vx, Nbins, 1);
    [nnvy, vy] = hist(data_all(idx,2)/std_vy, Nbins, 1);

    fx = mean(((data_all(:,1) - mean(data_all(:,1)))/std(data_all(:,1))).^4);
    fy = mean(((data_all(:,2) - mean(data_all(:,2)))/std(data_all(:,2))).^4);
    vpdf_flatness = [vpdf_flatness; nns(S), fx, fy];

    save('-ascii', [num2str(sideboxes(S)), outname_raw], 'data_all');
    hist_vel_component = [vx', nnvx', vy', nnvy'];
    save('-ascii', [num2str(sideboxes(S)), outname], 'hist_vel_component');
  endfor
  save('-ascii', flatness_outname, 'vpdf_flatness');
endfunction

function [cvxs, cvys] = nn_coarse_grain(data_xyvxvy, N)
  nn = (2*N+1)^2;

  P = size(data_xyvxvy, 1);
  cvxs = zeros(P, 1);
  cvys = zeros(P, 1);
  if(N == 0)
    cvxs = data_xyvxvy(:,3);
    cvys = data_xyvxvy(:,4);
  else
    d = 1/N;
    ixs = round(data_xyvxvy(:,1)*1000);
    iys = round(data_xyvxvy(:,2)*1000);
    for i=1:P
      ix = ceil(data_xyvxvy(i, 1)/d);
      iy = ceil(data_xyvxvy(i, 2)/d);
      cvxs((ix-1)*N + iy) += data_xyvxvy(i, 3);
      cvys((ix-1)*N + iy) += data_xyvxvy(i, 4);
      count_vy((ix-1)*N + iy) += 1;
      count_vx((ix-1)*N + iy) += 1;
    endfor

    cvxs = cvxs./count_vx;
    cvys = cvys./count_vy;
  endif
endfunction

function ix = periodic_index(i, L=1, U=1000)
  if(i >= L && i <= U)
    ix = i;
  elseif (i < L)
    ix = U + 1 - (L - i);
  elseif (i > U)
    ix = L + (i - U - 1);
  endif

endfunction
