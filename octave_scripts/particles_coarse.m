function particles_coarse(taus, N, Vabs, pat='part-*.dat')
  l = 1/sqrt(N);
  t2 = l/Vabs;
  flatness_np = [];

  files = dir(pat);
  Nfiles = length(files);

  K = length(taus);
  for k=1:K
      tau = taus(k);
      t1 = tau*t2;

      last_pos_x = [];
      last_pos_y = [];
      vxs = [];
      vys = [];

      avg_time = 0;
      prev_time = 0;
      Na = 0;
      
      data = load(files(1).name);
      avgvx = data(:,5);
      avgvy = data(:,6);
      Np = 0;
      for i=2:Nfiles
	data = load(files(i).name);
	dt = data(1,2) - prev_time;
	avgvx = avgvx + data(:,5);
	avgvy = avgvy + data(:,6);
	prev_time = data(1,2);
	avg_time = avg_time + dt;

	if(avg_time > t1)
	  Np = Np + 1;
	  vxs = [vxs;avgvx/Na];
	  vys = [vys;avgvy/Na];
	  avg_time = 0;
	  avgvx = data(:,5);
	  avgvy = data(:,6);
	endif
      endfor
      raw_data = [vxs, vys, sqrt(vxs.^2 + vys.^2)];

      taustr = num2str(tau);
      taustr(taustr == '.') = 'p';
      filename = ['part_tau', taustr, '_rawseries_v.dat'];
      save('-ascii', filename, 'raw_data');

      fx = mean(((vx - mean(vx))/std(vx)).^4);
      fy = mean(((vy - mean(vy))/std(vy)).^4);
      flatness_np = [flatness_np;tau, fx, fy, Np*N];
  endfor
  save('-ascii', 'flatness.txt', 'flatness_np');
endfunction
