function [xx, nn] = histogram_optim(data, range, Nbins=300)
%function [xx, nn] = histogram_optim(data, range, Nbins=300)
%histogram in -range*std ... range*std
  x = data(:,1);
  std_x = std(x);
  binsp = linspace(-range, range, Nbins);
  [nn, xx] = hist(x/std_x, binsp, 1);
  semilogy(xx, nn);
endfunction
