function timestep_stats(dts_file = 'timesteps.dat');
%function timesteps_stats(dts_file = 'timesteps.dat');
  data = load(dts_file);
  t = cumsum(data(:,2));
  dts = data(:,2);
  trapped = cumsum(data(:,6)) - cumsum(data(:,7));
  recs = cumsum(data(:,3));
  
  minr_vv = data(:,4);
  clear data;

  r = [t, dts, recs, trapped, minr_vv];
  f = fopen('timestep_stats.txt', 'w');
  fprintf(f, 't\tdts\trecs\ttrapped\tminr_vv\n');
  fclose(f);
  save('-ascii', '-append', 'timestep_stats.txt', 'r');
endfunction
