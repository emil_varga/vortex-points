function [sxs, sys] = stagnation_points(filename, M, maxmin)
  data = load(filename);

  xs = data(:,4:5);
  kappas = data(:,3);

  if(nargin < 2)
    M = 100;
  endif

  [xx, yy] = meshgrid(linspace(0,1,M), linspace(0,1,M));
  inits = [reshape(xx, M*M, 1), reshape(yy, M*M, 1)];
%  inits = rand(M,2);
  spoints = zeros(M*M,2);
  infos = [];
  
  f = @(x) F(x, xs, kappas);
  if(strcmp(maxmin, 'max'))
    f = @(x) -F(x,xs, kappas);
  endif

  for i=1:M*M
%    xmin = fminunc(@(x) F(x, xs, kappas), inits(i,:));
    [xmin, z, info] = sqp(inits(i,:)', f, [], [], [0,0]', [1,1]');
    infos = [infos; info];
    spoints(i,:) = xmin';
  endfor
  idx1 = spoints(:,1) < 1 & spoints(:,1) > 0;
  idx2 = spoints(:,2) < 1 & spoints(:,2) > 0;
  idx = idx1 & idx2 & (infos == 101);
  sxs = spoints(idx,1);
  sys = spoints(idx,2);
endfunction

function y = F(x, vxs, kappas)
  one = ones(size(vxs,1),1);
  zero = zeros(size(vxs,1),1);
  
  dxs0 = [one*x(1), one*x(2)] - vxs;
  dxs1 = dxs0 + [-one, zero];
  dxs2 = dxs0 + [one, zero];
  dxs3 = dxs0 + [zero, one];
  dxs4 = dxs0 + [zero, -one];

  sd0 = dxs0(:,1).^2 + dxs0(:,2).^2;
  sd1 = dxs1(:,1).^2 + dxs1(:,2).^2;
  sd2 = dxs2(:,1).^2 + dxs2(:,2).^2;
  sd3 = dxs3(:,1).^2 + dxs3(:,2).^2;
  sd4 = dxs4(:,1).^2 + dxs4(:,2).^2;

  sd0(sd0 < 1e-1) = 1;

  y = sum(kappas.*log(sd0.*sd1.*sd2.*sd3.*sd4));
endfunction
