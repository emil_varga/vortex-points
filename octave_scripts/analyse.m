%process the data
printf('particle stats\n');
particle_stats();
printf('vortex velocity stats\n');
vortex_vel_stats();
printf('vortex acceleration stats\n');
vortex_acc_stats();
printf('grid velocities\n');
velocity_pdf(300, './velocity-stats-mid-*0.dat');
printf('grid lagrangian accelerations\n');
laccel_pdf(300, './laccel-stats-*.dat');

%%%%%%basic statistics

analyse_fast;
