function angular_vortex_distribution(filepat, a_bins = 20)

  files = dir(filepat);
  N = length(files);

  as = linspace(-pi, pi, a_bins + 1);
  as = as(1:a_bins);
  bin_centers = as;

  data = load(files(1).name);
  Nvort = length(data(:,1));

  for i=1:N
    l = length(files(i).name);
    filename = [(files(i).name)(1:(l - 4)), '_avt.jpg'];
    if(!exist(filename, "file"))
      data = load(files(i).name);
      t = data(1, 2);
      sgn = data(:,3);
      xp = data(sgn>0, 4)-0.5; yp = data(sgn>0, 5)-0.5;
      xn = data(sgn<0, 4)-0.5; yn = data(sgn<0, 5)-0.5;

      ap = atan(yp./xp) + pi*0.5*(1 - sign(xp));
      an = atan(yn./xn) + pi*0.5*(1 - sign(xn));

      ap = to_mpipi(ap);
      an = to_mpipi(an);

      nsp = circ_hist(ap, bin_centers);
      nsn = circ_hist(an, bin_centers);
      bc = bin_centers;
      
      nsp = [nsp, nsp(1)];
      nsn = [nsn, nsn(1)];
      bc = [bc, bc(1)];

      cbc = cos(bc);
      sbc = sin(bc);

      plot(nsp.*cbc, nsp.*sbc, 'bo-',...
	   nsn.*cbc, nsn.*sbc, 'rx-',...
	   (nsp+nsn).*cbc, (nsp+nsn).*sbc, 'k*-');
      title(['time: ', sprintf("%.4f", t)]);
      axis([-Nvort/2, Nvort/2, -Nvort/2, Nvort/2]); 
      print(filename, '-djpg');
    endif
  endfor
  close all;
endfunction

function as = to_mpipi(as)

  N = length(as);
  for i=1:N
    while(as(i) > pi)
      as(i) -= 2*pi;
    endwhile
    while(as(i) < -pi)
      as(i) += 2*pi;
    endwhile
  endfor
endfunction

function ns = circ_hist(as, bcs)
  ns = zeros(size(bcs));
  N = length(as);
  M = length(bcs);
  for i=1:N
      [dummy, J] = min(abs(bcs - as(i)));
      if(J == M)
	[dummy, J] = min(abs([pi, bcs(M)] - as(i)));
	if(J==2)
	  J = M;
	endif
      endif
      ns(J)+=1;
  endfor
endfunction
