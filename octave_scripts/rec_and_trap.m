function rec_and_trap()
  data = load('timesteps.dat');
  time = cumsum(data(:,2));
  untrap = [];
  recs = cumsum(data(:,3));
  traps = cumsum(data(:,6));
  if(length(data(1,:)) > 6)
    untrap = cumsum(data(:,7));
  end
  plot(time, recs, time, traps, time(1:length(untrap)), untrap);
  recfreq = mean(recs./time);
  trapfreq = mean(traps./time);
  untrapfreq = 0
  if(~isempty(untrap))
    untrapfreq = mean(untrap./time);
  end
  printf('Reconection rate: %f\nTraping rate: %f\nUntraping rate: %f\n', recfreq, trapfreq, untrapfreq);

  axis([0, sum(data(:,2))]);
  ylabel('cummulative number of events');
  xlabel('time');
  legend('reconnections', 'trappings', 'untrapping');
  legend('location', 'northwest');
  print -deps -color 'rec_and_trap.eps';
end
