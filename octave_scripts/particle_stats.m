function particle_stats(pat = 'part-*.dat')
%function particle_stats(pat = 'part-*.dat')
  vortices = load('1-test00000.dat');

  v1files = dir('1-test*.dat');
  v2files = dir('2-test*.dat');
  v3files = dir('3-test*.dat');

  files = dir(pat);
  N = length(files);

  %all
  vxs = []; vys = [];
  axs = []; ays = [];

  %trapped
  tvxs = []; tvys = [];
  taxs = []; tays = [];

  %free
  fvxs = []; fvys = [];
  faxs = []; fays = [];
  traps = [];

  dtsdata = load('timesteps.dat');
  timesteps = dtsdata(:,2);
  times     = cumsum(timesteps);

  for i=1:N
    data = load(files(i).name);
    fidx = (data(:,9)==0);
    tidx = (data(:,9)!=0);
    vidx = data(tidx,9);

    vort1 = load(v1files(i).name);
    vort2 = load(v2files(i).name);
    vort3 = load(v3files(i).name);

    t2 = vort2(1,2);
    [tt, is] = min(abs(times - t2));
    dt1 = timesteps(is);
    dt2 = timesteps(is+1);
    vvx1 = vort1(vidx,6); vvy1 = vort1(vidx,7);
    vvx2 = vort2(vidx,6); vvy2 = vort2(vidx,7);
    vvx3 = vort3(vidx,6); vvy3 = vort3(vidx,7);

    taxs = [taxs; (dt1/dt2*(vvx3 - vvx2) - dt2/dt1*(vvx1 - vvx2))/(dt1+dt2)];
    tays = [tays; (dt1/dt2*(vvy3 - vvy2) - dt2/dt1*(vvy1 - vvy2))/(dt1+dt2)];

    tvxs = [tvxs; data(tidx,5)];
    tvys = [tvys; data(tidx,6)];
    traps = [traps; round(data(tidx, 9))];

    fvxs = [fvxs; data(fidx,5)];
    fvys = [fvys; data(fidx,6)];
    faxs = [faxs; data(fidx,7)];
    fays = [fays; data(fidx,8)];
  end

  Nvort = (size(data))(1);
  Nbins  = 400;

  vxs = [fvxs;tvxs];
  vys = [fvys;tvys];
  axs = [faxs;taxs];
  ays = [fays;tays];

  vs = sqrt(vxs.^2 + vys.^2);
  fvs = sqrt(fvxs.^2 + fvys.^2);
  tvs = sqrt(tvxs.^2 + tvys.^2);

  idx = vortices(traps,3)==1;
  tvxs(idx) = tvxs(idx) - mean(tvxs(idx));
  idx = vortices(traps,3)==-1;
  tvxs(idx) = tvxs(idx) - mean(tvxs(idx));

  vxs = [fvxs;tvxs];
  vys = [fvys;tvys];
  axs = [faxs;taxs];
  ays = [fays;tays];

  vs = sqrt(vxs.^2 + vys.^2);
  fvs = sqrt(fvxs.^2 + fvys.^2);
  tvs = sqrt(tvxs.^2 + tvys.^2);

  raw_all = [vxs, vys, vs];
  raw_free = [fvxs, fvys, fvs];
  raw_trap = [tvxs, tvys, tvs];

  save('-ascii', 'part_rawseries_all_v.dat', 'raw_all');
  save('-ascii', 'part_rawseries_free_v.dat', 'raw_free');
  save('-ascii', 'part_rawseries_trap_v.dat', 'raw_trap');

  std_vx  = std(vxs);
  std_fvx = std(fvxs);
  std_tvx = std(tvxs);

  std_vy  = std(vys);
  std_fvy = std(fvys);
  std_tvy = std(tvys);

  std_v  = std(vs);
  std_fv = std(fvs);
  std_tv = std(tvs);

  vys  = vys  - mean(vys);
  fvys = fvys - mean(fvys);
  tvys = tvys - mean(tvys);
  vxs  = vxs  - mean(vxs);
  fvxs = fvxs - mean(fvxs);
  tvxs = tvxs - mean(tvxs);

  binsp = linspace(-10*std_vx, 10*std_vx, Nbins);
  [nnvx, vx] = hist(vxs, binsp, 1);
  binsp = linspace(-10*std_fvx, 10*std_fvx, Nbins);
  [fnnvx, fvx] = hist(fvxs, binsp, 1);
  binsp = linspace(-10*std_tvx, 10*std_tvx, Nbins);
  [tnnvx, tvx] = hist(tvxs, binsp, 1);

  binsp = linspace(-10*std_vy, 10*std_vy, Nbins);
  [nnvy, vy] = hist(vys, binsp, 1);
  binsp = linspace(-10*std_fvy, 10*std_fvy, Nbins);
  [fnnvy, fvy] = hist(fvys, binsp, 1);
  binsp = linspace(-10*std_tvy, 10*std_tvy, Nbins);
  [tnnvy, tvy] = hist(tvys, binsp, 1);

  binsp = linspace(-10*std_v, 10*std_v, Nbins);
  [nnv, v] = hist(vs, binsp, 1);
  binsp = linspace(-10*std_fv, 10*std_fv, Nbins);
  [fnnv, fv] = hist(fvs, binsp, 1);
  binsp = linspace(-10*std_tv, 10*std_tv, Nbins);
  [tnnv, tv] = hist(tvs, binsp, 1);


  hist_all_v_component  = [vx'/std_vx, nnvx', vy'/std_vy, nnvy'];
  hist_trap_v_component = [tvx'/std_tvx, tnnvx', tvy'/std_tvy, tnnvy'];
  hist_free_v_component = [fvx'/std_fvx, fnnvx', fvy'/std_fvy, fnnvy'];
  hist_all_v_magnitude = [v', nnv'];
  hist_trap_v_magnitude = [tv', tnnv'];
  hist_free_v_magnitude = [fv', fnnv'];

  save('-ascii', 'part_hist_all_v_component.dat', 'hist_all_v_component');
  save('-ascii', 'part_hist_trap_v_component.dat', 'hist_trap_v_component');
  save('-ascii', 'part_hist_free_v_component.dat', 'hist_free_v_component');
  save('-ascii', 'part_hist_all_v_magnitude.dat', 'hist_all_v_magnitude');
  save('-ascii', 'part_hist_trap_v_magnitude.dat', 'hist_trap_v_magnitude');
  save('-ascii', 'part_hist_free_v_magnitude.dat', 'hist_free_v_magnitude');
    
  lvx = log(abs(vx)/std_vx);
  [lvx, is] = sort(lvx);
  lnnvx = log(nnvx(is));

  lfvx = log(abs(fvx)/std_fvx);
  [lfvx, is] = sort(lfvx);
  lfnnvx = log(fnnvx(is));

  ltvx = log(abs(tvx)/std_tvx);
  [ltvx, is] = sort(ltvx);
  ltnnvx = log(tnnvx(is));

  idx = lvx > 1 & ~isnan(lvx) & ~isnan(lnnvx) & ~isinf(lvx) & ~isinf(lnnvx);
  p_all = polyfit(lvx(idx), lnnvx(idx), 1);
  idx = lfvx > 0 & ~isnan(lfvx) & ~isnan(lfnnvx) & ~isinf(lfvx) & ~isinf(lfnnvx);
  p_free = polyfit(lfvx(idx), lfnnvx(idx), 1);
  idx = ltvx > 0 & ~isnan(ltvx) & ~isnan(ltnnvx) & ~isinf(ltvx) & ~isinf(ltnnvx);
  p_trap = polyfit(ltvx(idx), ltnnvx(idx), 1);

  figure; plot(lvx, lnnvx,...
	       lfvx, lfnnvx, 'linewidth', 1, 'linestyle', '--',...
	       ltvx, ltnnvx, 'linewidth', 1, 'linestyle', ':',...
	       lvx, p_all(1)*lvx + p_all(2), 'linewidth', 2.5,...
	       lfvx, p_free(1)*lfvx + p_free(2), 'linewidth', 2.5,...
	       ltvx, ltvx*p_trap(1) + p_trap(2), 'linewidth', 2.5);

  axis([-1, max(lvx), min(lnnvx), -2]);
  title('statistics of x component of tracer particle velocity');
  legend('all','free', 'trapped'...
	,[num2str(p_all(1)), ' power']...
	,[num2str(p_free(1)), ' power']...
	,[num2str(p_trap(1)), ' power']);
  xlabel('log(v/v_{sd})');
  ylabel('log(p.d.f.)');
  print('-deps', '-color', 'particle-vel-stats.eps');

  faxs = faxs(~isnan(faxs));
  fays = fays(~isnan(fays));
  taxs = taxs(~isnan(taxs));
  tays = tays(~isnan(tays));
  axs = axs(~isnan(axs));
  ays = ays(~isnan(ays));

  as = sqrt(axs.^2 + ays.^2);
  tas = sqrt(taxs.^2 + tays.^2);
  fas = sqrt(faxs.^2 + fays.^2);

  raw_all = [axs, ays, as];
  raw_free = [faxs, fays, fas];
  raw_trap = [taxs, tays, tas];

  save('-ascii', 'part_rawseries_all_a.dat', 'raw_all');
  save('-ascii', 'part_rawseries_free_a.dat', 'raw_free');
  save('-ascii', 'part_rawseries_trap_a.dat', 'raw_trap');

  std_ax =  std(axs);
  std_tax = std(taxs);
  std_fax = std(faxs);

  std_ay =  std(ays);
  std_tay = std(tays);
  std_fay = std(fays);

  std_a =  std(as);
  std_ta = std(tas);
  std_fa = std(fas);

  binsp = linspace(-5*std_a, 5*std_a, Nbins);
  [nna, a] = hist(as, binsp, 1);
  binsp = linspace(-5*std_ta, 5*std_ta, Nbins);
  [tnna, ta] = hist(tas, binsp, 1);
  binsp = linspace(-5*std_fa, 5*std_fa, Nbins);
  [fnna, fa] = hist(fas, binsp, 1);

  binsp = linspace(-5*std_fay, 5*std_fay, Nbins);
  [fnnay, fay] = hist(fays, binsp, 1);
  binsp = linspace(-5*std_fax, 5*std_fax, Nbins);
  [fnnax, fax] = hist(faxs, binsp, 1);
  lfax = log(abs(fax)/std_fax);
  [lfax, is] = sort(lfax);
  lfnnax = log(fnnax(is));

  binsp = linspace(-5*std_tay, 5*std_tay, Nbins);
  [tnnay, tay] = hist(tays, binsp, 1);
  binsp = linspace(-5*std_tax, 5*std_tax, Nbins);
  [tnnax, tax] = hist(taxs, binsp, 1);
  ltax = log(abs(tax)/std_tax);
  [ltax, is] = sort(ltax);
  ltnnax = log(tnnax(is));

  binsp = linspace(-5*std_ay, 5*std_ay, Nbins);
  [nnay, ay] = hist(ays, binsp, 1);
  binsp = linspace(-5*std_ax, 5*std_ax, Nbins);
  [nnax, ax] = hist(axs, binsp, 1);
  lax = log(abs(ax)/std_ax);
  [lax, is] = sort(lax);
  lnnax = log(nnax(is));

  hist_all_a_component  = [ax'/std_ax, nnax', ay'/std_ay, nnay'];
  hist_trap_a_component = [tax'/std_tax, tnnax', tay'/std_tay, tnnay'];
  hist_free_a_component = [fax'/std_fax, fnnax', fay'/std_fay, fnnay'];
  hist_all_a_magnitude = [a', nna'];
  hist_trap_a_magnitude = [ta', tnna'];
  hist_free_a_magnitude = [fa', fnna'];

  save('-ascii', 'part_hist_all_a_component.dat', 'hist_all_a_component');
  save('-ascii', 'part_hist_trap_a_component.dat', 'hist_trap_a_component');
  save('-ascii', 'part_hist_free_a_component.dat', 'hist_free_a_component');
  save('-ascii', 'part_hist_all_a_magnitude.dat', 'hist_all_a_magnitude');
  save('-ascii', 'part_hist_trap_a_magnitude.dat', 'hist_trap_a_magnitude');
  save('-ascii', 'part_hist_free_a_magnitude.dat', 'hist_free_a_magnitude');

  idx = lax > -1 & ~isnan(lax) & ~isnan(lnnax) & ~isinf(lax) & ~isinf(lnnax);
  p_all = polyfit(lax(idx), lnnax(idx), 1);
  idx = lfax > -1 & ~isnan(lfax) & ~isnan(lfnnax) & ~isinf(lfax) & ~isinf(lfnnax);
  p_free = polyfit(lfax(idx), lfnnax(idx), 1);

  figure; plot(lax, lnnax,...
	       lfax, lfnnax, 'linewidth', 1, 'linestyle', '--',...
	       ltax, ltnnax, 'linewidth', 1, 'linestyle', ':',...
	       lax, lax*p_all(1) + p_all(2), lfax, lfax*p_free(1) + p_free(2));
  rng = axis;
  rng(4) = max([lnnax, lfnnax, ltnnax]) + 1;
  axis(rng);
  title('statistics of x-component of tracer particle acceleration');
  legend('all', 'free', 'trapped',...
	[num2str(p_all(1)), ' power'],...
	[num2str(p_free(1)), ' power']);
  legend('location', 'southwest');
  xlabel('log(a/a_{sd})');
  ylabel('log(p.d.f.)');
  print('-deps', '-color', 'particle-acc-stats.eps');
endfunction
