module Init
  implicit none
  real, parameter :: PI = 3.141597
  contains    
    subroutine initialize_random(vort_circ, vort_pos)
      implicit none
      real, dimension(:,:), intent(out) :: vort_pos
      integer, dimension(size(vort_pos,2)), intent(out) :: vort_circ
      integer :: N
      
      integer :: up, down
      integer :: i
      real :: tmp

      call init_random_seed

      N = size(vort_pos,2)
      up = N/2; down = N/2;
      if (up == 0 .AND. down == 0) up = 1
      
      call random_number(vort_pos)

      do i = 1,N
         if ((up > 0) .AND. (down > 0)) then
            call random_number(tmp)
            if (tmp > 0.5) then
               up = up - 1
               vort_circ(i) = 1
            else
               down = down - 1
               vort_circ(i) = -1
            endif
         elseif (up > 0) then
            up = up - 1
            vort_circ(i) = 1
         elseif (down > 0) then
            down = down - 1
            vort_circ(i) = -1
         else
            print *, "Problem initializing!"
            call exit(-1)
         end if
      end do
    end subroutine initialize_random

    subroutine make_cluster_strict(x, y, bbox_side, vort_pos)
      real, dimension(:,:), intent(out) :: vort_pos
      real, intent(in) :: x, y, bbox_side

      integer :: N, layer, circ, layers, k, rest
      real :: L, lr

      N = size(vort_pos,2);
      if(N==1) then
         vort_pos(:,1) = [x,y]
         return
      end if

      !with (5+layer) points per layer (indexed from zero), when fully filled
      !L layers will contain (L+1)*5 +L*(L+1)/2 = N
      L = 0.5*(-11.0 + sqrt(41.0 + 16.0*N))
      if(L < 1.0) then !L can be negative
         layers = 1
      else
         layers = ceiling(L)
      end if
      
      lr = 0.5*bbox_side/layers
      k = 0
      do layer = 1,layers
         if(N-k > 4+layer) then
            do circ = 1,(4+layer)
               vort_pos(:,k) = layer*lr*[cos(2*PI/(4+layer)*circ)&
                    &                   ,sin(2*PI/(4+layer)*circ)]
               k = k+1
            end do
         else
            rest = N-k
            do circ = 1,rest
               vort_pos(:,k) = layer*lr*[cos(2*PI/rest*circ)&
                    &                   ,sin(2*PI/rest*circ)]
               k = k+1
            end do
         end if
      end do
    end subroutine make_cluster_strict

    
    subroutine make_cluster_gaussian(xc, yc, s, vort_pos)
      !this subroutine does not return vortex positions strictly
      !in (0,1)^2. The positions must be manually coerced with periodic_coerce
      !from Vortices module
      real, intent(in)                  :: xc, yc, s
      real, dimension(:,:), intent(out) :: vort_pos
      real, dimension(size(vort_pos, 1), size(vort_pos, 2)) :: tmp
      integer :: N

      N = size(vort_pos, 2);
      call init_random_seed;
      call random_number(tmp);

      !simple Box-Muller transform to get a Gaussian-shaped vortex cluster

      vort_pos(1,:) = xc + s*sqrt(-log(tmp(1,:))/2)*cos(2*PI*tmp(2,:));
      vort_pos(2,:) = yc + s*sqrt(-log(tmp(1,:))/2)*sin(2*PI*tmp(2,:));
      
    end subroutine make_cluster_gaussian
    
    subroutine initialize_random_particles(particle_pos_vel, vn)
      real, dimension(:,:), intent(out) :: particle_pos_vel
      real, dimension(2)                :: vn

      integer :: N
      N = size(particle_pos_vel,2)/2

      particle_pos_vel(1,(N+1):(2*N)) = vn(1)
      particle_pos_vel(2,(N+1):(2*N)) = vn(2)

      call random_number(particle_pos_vel(:,1:N))
    end subroutine initialize_random_particles

    subroutine init_random_seed()
      implicit none
      integer, allocatable :: seed(:)
      integer :: i, n, un, istat, dt(8), pid, t(2), s
      integer(8) :: count, tms
          
      call random_seed(size = n)
      allocate(seed(n))
      ! First try if the OS provides a random number generator
      open(newunit=un, file="/dev/urandom", access="stream", &
           form="unformatted", action="read", status="old", iostat=istat)
      if (istat == 0) then
         read(un) seed
         close(un)
      else
         ! Fallback to XOR:ing the current time and pid. The PID is
         ! useful in case one launches multiple instances of the same
         ! program in parallel.
         call system_clock(count)
         if (count /= 0) then
            t = transfer(count, t)
         else
            call date_and_time(values=dt)
            tms = (dt(1) - 1970) * 365_8 * 24 * 60 * 60 * 1000 &
                 + dt(2) * 31_8 * 24 * 60 * 60 * 1000 &
                 + dt(3) * 24 * 60 * 60 * 60 * 1000 &
                 + dt(5) * 60 * 60 * 1000 &
                 + dt(6) * 60 * 1000 + dt(7) * 1000 &
                 + dt(8)
            t = transfer(tms, t)
         end if
         s = ieor(t(1), t(2))
         pid = getpid() + 1099279 ! Add a prime
         s = ieor(s, pid)
         if (n >= 3) then
            seed(1) = t(1) + 36269
            seed(2) = t(2) + 72551
            seed(3) = pid
            if (n > 3) then
               seed(4:) = s + 37 * (/ (i, i = 0, n - 4) /)
            end if
         else
            seed = s + 37 * (/ (i, i = 0, n - 1 ) /)
         end if
      end if
      call random_seed(put=seed)
    end subroutine init_random_seed
    
end module Init
