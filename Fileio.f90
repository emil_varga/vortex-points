module FileIO

  contains

    function get_arguments(N, vsx, vnx, alpha, alphap, &
         &P, tau_visc, rhoN, rhoS, rho0, rec_l, trap_l, &
         &cosprof_ampl, cosprof_periods) result(num_args)
      real, intent(out)               :: alpha, alphap
      real, dimension(2), intent(out) :: vsx, vnx
      integer, intent(out)            :: N
      integer, intent(out), optional  :: P, cosprof_periods
      real, intent(out), optional :: tau_visc, rhoN, rhoS, rho0, rec_l, trap_l, cosprof_ampl
      character (len=128) :: filename
      integer :: num_args

      num_args = command_argument_count()
      
      if(num_args > 0) then
         call get_command_argument(1, filename)
         open(1, FILE=filename, status='OLD', action='READ')
         read (1,*) N
         read (1,*) vsx
         read (1,*) vnx
         if(present(cosprof_ampl).and.present(cosprof_periods)) then
            read (1,*) cosprof_ampl, cosprof_periods
         end if
         read (1,*) alpha
         read (1,*) alphap
         if(present(P)) read (1,*) P
         if(present(tau_visc)) read (1,*) tau_visc
         if(present(rhoN)) read (1,*) rhoN
         if(present(rhoS)) read (1,*) rhoS
         if(present(rho0)) read (1,*) rho0
         if(present(rec_l)) read (1,*) rec_l
         if(present(trap_l)) read (1,*) trap_l
         close(1)
      end if
    end function get_arguments
    
    subroutine save_snapshot(frame, time, prefix, vort_pos, vort_circ, vort_v, dists)
      integer, intent(in)                 :: frame
      real, intent(in)                    :: time
      character(len=*), intent(in)        :: prefix
      real, dimension(:,:), intent(in)    :: vort_pos, vort_v
      integer, dimension(:), intent(in)   :: vort_circ
      real, dimension(:), intent(in), optional :: dists

      character(len=128)      :: filename
      integer :: i

      write(filename, '(A,I5.5,A)') prefix, frame, '.dat'
      open(unit=99, file=filename, status='NEW')
      
      write (99,*) '#', size(vort_circ)
      if(present(dists)) then 
         do i=1,size(vort_pos,2)
            write (99,*) i, time, vort_circ(i), vort_pos(:,i),  vort_v(:,i), dists(i)
         end do
      else
         do i=1,size(vort_pos,2)
            write (99,*) i, time, vort_circ(i), vort_pos(:,i),  vort_v(:,i)
         end do
      end if
      close(99)
    end subroutine save_snapshot

    function load_snapshot(filename, vort_circ, vort_pos, vort_v, time, N) result (stat)
      integer :: stat !status
      integer, dimension(:), intent(out) :: vort_circ
      real, dimension(:,:), intent(out)  :: vort_pos, vort_v
      real, intent(out) :: time

      integer, intent(in) :: N

      character(len=*), intent(in) :: filename
      integer :: N_found, curr_vc, i, tmpi
      real    :: curr_vpx, curr_vpy, curr_vvx, curr_vvy
      character :: tmp
      stat = 0

      open(unit=99, file=filename, status='OLD', action='READ')
      read (99,*) tmp, N_found

      if(N_found /= N) then
         !the number of vortices in the config file and in snapshot file are not the same
         !cannot continue -- parameters are not known
         stat = -1 
         return
      end if
      
      do i=1,N
         read (99, *) tmpi, time, curr_vc, curr_vpx, curr_vpy, curr_vvx, curr_vvy
         vort_circ(i) = curr_vc
         vort_pos(:,i) = [curr_vpx, curr_vpy]
         vort_v(:,i) = [curr_vvx, curr_vvy]
      end do
      close(99)
      
    end function load_snapshot
    
    subroutine save_snapshot_particles(frame, time, prefix, particles, pvs, trapped, vp_dists)
      integer, intent(in)              :: frame, trapped(:)
      real, intent(in)                 :: time
      character(len=*), intent(in)     :: prefix
      real, dimension(:,:), intent(in) :: particles, pvs
      real, dimension(:), intent(in), optional   :: vp_dists

      character(len=128) :: filename
      integer            :: i, P

      P = size(particles,2)/2
      if(P==0) return !no particles, don't save anything

      write(filename, '(A,I5.5,A)') prefix, frame, '.dat'
      open(unit=99, file=filename, status='NEW')
      
      write (99,*) '#', P
      if(present(vp_dists)) then
         do i=1,P
            write (99,*) i, time, particles(:,i), pvs(:,i), pvs(:,P+i), trapped(i), vp_dists(i)
         end do
      else
         do i=1,P
            write (99,*) i, time, particles(:,i), pvs(:,i), pvs(:,P+i), trapped(i)
         end do
      end if
      close(99)
    end subroutine save_snapshot_particles

    function load_snapshot_particles(part_posvel, part_velacc, trapped, pv_dist, time, P) result(stat)
      integer :: stat !status
      real, dimension(:,:), intent(out)  :: part_posvel, part_velacc
      integer, dimension(:), intent(out) :: trapped
      real, dimension(:), intent(out) :: pv_dist
      real, intent(out) :: time
      integer, intent(in) :: P
      character(len=128) :: filename
      integer :: P_found, trap, i, tmpi
      real    :: curr_px, curr_py, curr_vx, curr_vy, curr_ax, curr_ay, dist
      character :: tmp
      stat = 0

      call get_command_argument(3, filename)
      open(unit=99, file=filename, status='OLD', action='READ')
      read (99,*) tmp, P_found

      if(P_found /= P) then
         !the number of particles in the config file and in snapshot file are not the same
         !cannot continue -- parameters are not known
         stat = -1 
         return
      end if
      
      do i=1,P
         read (99, *) tmpi, time, curr_px, curr_py, curr_vx, curr_vy, curr_ax, curr_ay,&
              & trap, dist
         trapped(i) = trap
         pv_dist(i) = dist
         part_posvel(:,i) = [curr_px, curr_py]
         part_posvel(:,P+i) = [curr_vx, curr_vy]
         part_velacc(:,i) = [curr_vx, curr_vy]
         part_velacc(:,P+i) = [curr_ax, curr_ay]
      end do
      
    end function load_snapshot_particles

end module FileIO
