program VsDist
  use FileIO
  use Vortices
  use Particles
  use external_flow
  use omp_lib
  implicit none

  !particle parameters
  real :: tau_visc = 2.9241e-4, rho0 = 1.5, rhos = 0.268, rhon = 0.732

  !calculation parameters
  integer            :: N, P !number of vortex points and particles
  real    :: rec_l  !reconnection length
  real    :: trap_l ! trapping length

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v
   integer, dimension(:), allocatable   :: vort_circ

  !distributions
  integer, parameter :: npdf = 100
  real, dimension(:,:), allocatable :: grid
  real, dimension(:,:), allocatable :: vels
  real, dimension(:), allocatable :: point_vort_dists
  integer, dimension(:), allocatable :: dummy

  integer :: num_args, i, k, Nbins, current_bin
  real :: time
  character(len=128) :: arg, filename
  real, dimension(:), allocatable :: vs_dist
  integer, dimension(:), allocatable :: vdist_counts
  logical :: out_file_exists

  !the normal fluid cosine profile, unused at the moment
  integer :: periods
  real :: ampl

  call get_command_argument(2, arg)
  filename = "vs_dist_" // arg
  inquire(file=filename, exist=out_file_exists)
  if(.not.out_file_exists) then     
     allocate(grid(2,npdf*npdf), vels(2,npdf*npdf), point_vort_dists(npdf*npdf), dummy(npdf*npdf))

     dummy = 0

     do i=1,npdf
        do k=1,npdf
           grid(:, (i-1)*npdf + k) = [(1.0*i)/npdf, (1.0*k)/npdf]
        end do
     end do

     num_args = get_arguments(N, vsx, vnx, alpha, alphap&
          &,P, tau_visc, rhoN, rhoS, rho0, rec_l, trap_l,&
       &cosprof_ampl=ampl, cosprof_periods=periods)
     if(num_args < 1) then
        print *, 'Missing name of file with parameters'
        call exit(-1)
     endif
     call get_command_argument(2, filename)

     allocate(vort_pos(2,N), vort_v(2,N), vort_circ(N))
     if(load_snapshot(filename, vort_circ, vort_pos, vort_v, time, N) < 0) then
        print *, 'Error loading snapshot'
        call exit(-1)
     end if

     if(num_args < 3) then
        Nbins = 64
     else
        call get_command_argument(3, arg)
        read(arg, *) Nbins
     end if
     allocate(vs_dist(Nbins), vdist_counts(Nbins))

     call set_vsexternal(vsx, vnx)

     call set_mutual_friction(alpha, alphap)

     call compute_velocities(grid, vort_circ, vort_pos, vels)
     call compute_pv_dists(vort_pos, grid, point_vort_dists)
     current_bin = 1
     vs_dist = 0; vdist_counts = 0;
     do i=1, npdf
        if(grid(1, 1 + (i-1)*npdf) > current_bin/float(Nbins)) current_bin = current_bin + 1

        vdist_counts(current_bin) = vdist_counts(current_bin) + 1
        vs_dist(current_bin) = vs_dist(current_bin) + sum(vels(2,(1+(i-1)*npdf):(i*npdf))/npdf)
     end do
     vs_dist = vs_dist / vdist_counts
     call get_command_argument(2, arg)
     open(unit=10, FILE=filename, status='NEW', action='WRITE')
     do i=1, Nbins
        write(10,*) (i-0.5)/Nbins, vs_dist(i)
     end do
     close(10)

     deallocate(grid, vels, point_vort_dists, dummy)
     deallocate(vort_pos, vort_v, vort_circ)
     deallocate(vs_dist, vdist_counts)
  end if
end program VsDist
