program SnapField
  use FileIO
  use Vortices
  use Particles
  use external_flow
  use omp_lib
  implicit none

  !particle parameters
  real :: tau_visc = 2.9241e-4, rho0 = 1.5, rhos = 0.268, rhon = 0.732

  !calculation parameters
  integer            :: N, P !number of vortex points and particles
  real    :: rec_l  !reconnection length
  real    :: trap_l ! trapping length

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v
   integer, dimension(:), allocatable   :: vort_circ

  !distributions
  integer, parameter :: npdf = 1000
  real, dimension(:,:), allocatable :: grid
  real, dimension(:,:), allocatable :: vels
  real, dimension(:), allocatable :: point_vort_dists
  integer, dimension(:), allocatable :: dummy

  integer :: num_args, i, k
  real :: time
  character(len=128) :: filename

  !normal fluid velocity profile
  real :: cosprof_ampl
  integer :: cosprof_periods

  print *, 'Starting'
  
  allocate(grid(2,npdf*npdf), vels(2,npdf*npdf), point_vort_dists(npdf*npdf), dummy(npdf*npdf))

  dummy = 0
  
  do i=1,npdf
     do k=1,npdf
        grid(:, (i-1)*npdf + k) = [(1.0*i)/npdf, (1.0*k)/npdf]
     end do
  end do
  
  num_args = get_arguments(N, vsx, vnx, alpha, alphap&
       &,P, tau_visc, rhoN, rhoS, rho0, rec_l, trap_l,&
       &cosprof_ampl=cosprof_ampl, cosprof_periods=cosprof_periods)

  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif

  call set_cos_vnprofile(cosprof_periods, cosprof_ampl)

  call get_command_argument(2, filename)

  allocate(vort_pos(2,N), vort_v(2,N), vort_circ(N))
  if(load_snapshot(filename, vort_circ, vort_pos, vort_v, time, N) < 0) then
     print *, 'Error loading snapshot'
     call exit(-1)
  end if
  
  call set_vsexternal(vsx, vnx)

  call set_mutual_friction(alpha, alphap)

  call compute_velocities(grid, vort_circ, vort_pos, vels)
  call compute_pv_dists(vort_pos, grid, point_vort_dists)
  print *, 'Saving velocity stats'
  call save_snapshot(999, time, 'velocity-stats-mid-', grid, dummy, vels, point_vort_dists)
  vels = mat_diff(grid, vort_pos, vort_v, vort_circ)
  print *, 'Saving laccel stats'
  call save_snapshot(999, time, 'laccel-stats-', grid, dummy, vels, point_vort_dists);
  
  print *, 'Exiting.'
  deallocate(vort_pos, vort_v, vort_circ)

end program SnapField
