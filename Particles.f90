module Particles
  use Vortices
  use external_flow
  use stepping

  real :: visc_relax_time, rho0, rho_n, rho_s

  private visc_relax_time, rho0, rho_n, rho_s
  contains
    
    subroutine set_particle_params(tau, r0, rn, rs)
      real, intent(in) :: tau, r0, rn, rs
      visc_relax_time = tau
      rho0 = r0
      rho_n = rn
      rho_s = rs
    end subroutine set_particle_params

    function trap_particles(particles, trapped, vort_pos, vort_vel, trapping_distance, mindists) result(m)
      real, dimension(:,:), intent(in) :: vort_pos, vort_vel
      real, dimension(:,:), intent(inout) :: particles
      integer, dimension(:), intent(inout) :: trapped
      real, intent(in) :: trapping_distance
      real, dimension(:), intent(out), optional :: mindists

      integer :: V, P, i, j, m
      real    :: r2, trapr2, dx, dy, minr
      trapr2 = trapping_distance**2

      if(present(mindists)) mindists = 0

      V = size(vort_pos, 2)
      P = size(particles,2)/2
      m = 0
      do j=1,P
         if(trapped(j) == 0) then
            minr = 1
            do i=1,V
               dx = abs(vort_pos(1,i) - particles(1,j))
               dy = abs(vort_pos(2,i) - particles(2,j))
               dx = min(dx, 1-dx); dy = min(dy, 1-dy)
               r2 = dx**2 + dy**2
               if(r2 < minr) minr = r2
               if(r2 < trapr2) then
                  m = m + 1
                  particles(:,j)   = vort_pos(:,i)
                  particles(:,P+j) = vort_vel(:,i)
                  trapped(j) = i
                  exit
               end if
            end do
            if(trapped(j)==0 .and. present(mindists)) then
               mindists(j) = sqrt(minr)
            end if
         end if
      end do            
    end function trap_particles

    subroutine untrap_particles(trapped, vortex_anihilations, m)
      integer, dimension(:), intent(in) :: vortex_anihilations
      integer, dimension(:), intent(inout) :: trapped
      integer, intent(out), optional :: m

      integer :: N, VA, i, j, tmpm
      N = size(trapped)
      VA = size(vortex_anihilations)
      tmpm = 0
      do i=1,N
         do j=1,VA
            if(trapped(i)/=0 .and. trapped(i) == vortex_anihilations(j)) then
               trapped(i) = 0
               tmpm = tmpm+1
            end if
         end do
      end do
      if(present(m)) m = tmpm
    end subroutine untrap_particles
        
    function particle_rhs(particles, trapped, vort_pos, vort_vel, vort_circ, delta) result(rhs)
      real, dimension(:,:), intent(in)     :: particles
      real, dimension(:,:), intent(in)  :: vort_pos, vort_vel
      integer, dimension(:), intent(in) :: vort_circ, trapped
      real, dimension(size(particles,1), size(particles,2)) :: rhs
      real, intent(out), optional :: delta
      
      real :: tmpdelta

      integer :: N, i, sec_trap(size(trapped)), sec_free(size(trapped)), Nfree, Ntrapped
      N = size(particles,2)/2
      sec_trap = 0
      sec_free = 0
      tmpdelta = 1
      !connstructing free and trapped sections of the particle array
      Nfree=0; Ntrapped=0;
      do i=1,N
         if(trapped(i) == 0) then
            Nfree = Nfree + 1
            sec_free(Nfree) = i
         else
            Ntrapped = Ntrapped + 1
            sec_trap(Ntrapped) = i
         end if
      end do
      
      if(Nfree > 0) then
         rhs(:,sec_free(1:Nfree)) = particles(:, N+sec_free(1:Nfree))
         rhs(:,N+sec_free(1:Nfree)) = part_accel(particles(:,sec_free(1:Nfree)),&
              &particles(:,N+sec_free(1:Nfree)),&
              &vort_pos, vort_vel, vort_circ, tmpdelta)
      else
         rhs = 0
      end if

      do i=1,Ntrapped
         rhs(:,N+sec_trap(i)) = 0
         rhs(:,sec_trap(i)) = vort_vel(:,trapped(sec_trap(i)))
      end do
      
      if(present(delta)) delta = tmpdelta
    end function particle_rhs
    
    function part_accel(part_pos, part_vel, vort_pos, vort_vel, vort_circ, delta) result (ap)
      real, dimension(:,:), intent(in)  :: part_pos, part_vel, vort_pos, vort_vel
      integer, dimension(:), intent(in) :: vort_circ
      real, intent(out), optional :: delta
      real :: tmpdelta

      real, dimension(2, size(part_pos,2)) :: vn, DvnDt

      real, dimension(size(part_pos,1), size(part_pos,2)) :: ap
      real, dimension(size(part_pos,1), size(part_pos,2)) :: matder

      vn = get_evn(part_pos)
      DvnDt = get_evn_matdiff(part_pos)

      matder = mat_diff(part_pos, vort_pos, vort_vel, vort_circ, tmpdelta);
      if(present(delta)) delta = tmpdelta
      
      ap = (vn - part_vel)/visc_relax_time
      ap = ap + 1.5/rho0*(rho_s*matder + rho_n*DvnDt)
    end function part_accel
    
    function mat_diff(rs, vort_pos, vort_vel, vort_circ, delta) result (DvsDt)
      real, intent(in), dimension(:,:)                 :: rs, vort_pos, vort_vel
      integer, intent(in), dimension(size(vort_pos,2)) :: vort_circ
      real, dimension(size(rs,1), size(rs,2))          :: DvsDt, dvsdtp, vsgvs
      real, intent(out), optional :: delta
      real :: tmpdelta

      integer :: N,i
      real, dimension(size(vort_pos,2)) :: dists2, distsx(2,size(vort_pos,2)), distsy(2, size(vort_pos,2))
      real, dimension(size(rs,1), size(rs,2)) :: point_vels
      real                              :: dxvx, dxvy
      !dyvy = - dxvx and dxvy = dyvx; divj = d(v_j)/di

      call compute_velocities(rs, vort_circ, vort_pos, point_vels, tmpdelta)
      if(present(delta)) delta = tmpdelta

      N = size(rs,2);
      !$omp parallel do schedule(dynamic, N/4) default(shared) &
      !$omp& private(i,distsx, distsy, dists2, dxvx, dxvy)
      do i=1,N
         distsx(1,:) = abs(vort_pos(1,:) - rs(1,i))
         distsx(2,:) = 1 - distsx(1,:)
         distsy(1,:) = abs(vort_pos(2,:) - rs(2,i))
         distsy(2,:) = 1 - distsy(1,:)
         
         distsx(1,:) = minval(distsx, DIM=1)
         distsy(1,:) = minval(distsy, DIM=1)
         dists2 = distsx(1,:)**2 + distsy(1,:)**2

         !The following code assumes that external superfluid velocity is flat and constant.
         !Turbulent external superfluid velocity would be superflous because its only source
         !should be the vortices, which are calculated.

         dvsdtp(1,i) = 0.5/pi*sum(vort_circ*vort_vel(2,:)/dists2 - vort_circ*(rs(2,i) - vort_pos(2,:))&
              &/dists2**2*((rs(1,i) - vort_pos(1,:))*vort_vel(1,:)+(rs(2,i) - vort_pos(2,:))*vort_vel(2,:)))

         dvsdtp(2,i) = 0.5/pi*sum(-vort_circ*vort_vel(1,:)/dists2 + vort_circ*(rs(1,i) - vort_pos(1,:))&
              &/dists2**2*((rs(1,i) - vort_pos(1,:))*vort_vel(1,:)+(rs(2,i) - vort_pos(2,:))*vort_vel(2,:)))

         dxvx = sum(vort_circ*(rs(1,i) - vort_pos(1,:))*(rs(2,i) - vort_pos(2,:))/dists2**2)/pi
         dxvy = 0.5/pi*sum(vort_circ/dists2**2*((rs(2,i) - vort_pos(2,:))**2 - (rs(1,i) - vort_pos(1,:))**2))

         vsgvs(:,i) = [point_vels(1,i)*dxvx + point_vels(2,i)*dxvy, &
              &        point_vels(1,i)*dxvy - point_vels(2,i)*dxvx]
      end do
      !$omp end parallel do

      DvsDt = dvsdtp + vsgvs
    end function mat_diff
    
    function dvs_dt(rs, vort_pos, vort_vel, vort_circ) result (dvsdt)
      real, intent(in), dimension(:,:)                 :: rs, vort_pos, vort_vel
      integer, intent(in), dimension(size(vort_pos,2)) :: vort_circ
      real, dimension(size(rs,1), size(rs,2))          :: dvsdt

      integer :: N,i
      real, dimension(size(vort_pos,2)) :: dists2

      N = size(rs,2);
      do i=1,N
         dists2 = (vort_pos(2,:) - rs(2,i))**2 + (vort_pos(2,:) - rs(2,i))**2

         dvsdt(1,i) = 0.5/pi*sum(vort_circ*vort_vel(2,:)/dists2 - vort_circ*(rs(2,i) - vort_pos(2,:))&
              &/dists2**2*((rs(1,i) - vort_pos(1,:))*vort_pos(1,:)+(rs(2,i) - vort_pos(2,:))*vort_pos(2,:)))
         dvsdt(2,i) = 0.5/pi*sum(-vort_circ*vort_vel(1,:)/dists2 + vort_circ*(rs(1,i) - vort_pos(1,:))&
              &/dists2**2*((rs(1,i) - vort_pos(1,:))*vort_pos(1,:)+(rs(2,i) - vort_pos(2,:))*vort_pos(2,:)))
      end do
    end function dvs_dt

    function vsgrad_vs(rs, vort_pos, vort_circ) result(vsgvs)
      real, intent(in), dimension(:,:)                 :: rs, vort_pos
      integer, intent(in), dimension(size(vort_pos,2)) :: vort_circ
      real, dimension(size(rs,1), size(rs,2))          :: vsgvs

      real, dimension(size(rs,1), size(rs,2)) :: point_vels
      real, dimension(size(vort_pos,2)) :: dists2
      real                              :: dxvx, dxvy
      !dyvy = - dxvx and dxvy = dyvx; divj = d(v_j)/di
      integer :: N,i

      call compute_velocities(rs, vort_circ, vort_pos, point_vels)

      N = size(rs,2)
      do i=1,N
         dists2 = (vort_pos(1,:) - rs(1,i))**2 + (vort_pos(2,:) - rs(2,i))**2
         dxvx = sum(vort_circ*(rs(1,i) - vort_pos(1,:))*(rs(2,i) - vort_pos(2,:))/dists2**2)/pi
         dxvy = 0.5/pi*sum(vort_circ/dists2**2*((rs(2,i) - vort_pos(2,:))**2 - (rs(1,i) - vort_pos(1,:))**2))

         vsgvs(:,i) = [point_vels(1,i)*dxvx + point_vels(2,i)*dxvy, &
              &point_vels(1,i)*dxvy - point_vels(2,i)*dxvx]
      end do
    end function vsgrad_vs
    
end module Particles
