module Vortices
  use stepping
  use external_flow
  implicit none
  integer, parameter :: periodic_boxes = 4
  real, parameter    :: pi = 3.14159

  real :: alpha = 0, alphap = 0
  logical :: mf = .false.

  interface
     function vortex_field_interface(x, y, vortex_circ, vortex_x, vortex_y, r) result(v)
      real, intent(in)            :: x, y, vortex_x, vortex_y
      integer, intent(in)         :: vortex_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v
    end function vortex_field_interface

  end interface

  procedure(vortex_field_interface), pointer :: vortex_field => periodic_vortex_field
  procedure(vortex_field_interface), pointer :: shadow_vortex_field => periodic_shadow_vortex_field
 
  private alpha, alphap, periodic_boxes, vortex_field, shadow_vortex_field
contains

  subroutine set_vortex_field_function(f)
    procedure(vortex_field_interface) ::  f

    vortex_field => f
  end subroutine set_vortex_field_function

  subroutine set_shadow_vortex_field_function(f)
    procedure(vortex_field_interface) ::  f

    shadow_vortex_field => f
  end subroutine set_shadow_vortex_field_function

        
  function step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta) result(dx)
      real, intent(in)                    :: dtinit
      integer, dimension(:), intent(in)   :: vort_circ
      real, dimension(:,:), intent(in)    :: vort_pos
      real, dimension(size(vort_pos,1),size(vort_pos,2)), intent(out)  :: vort_v
      real, intent(out)                  :: dtused
      real, dimension(size(vort_pos,1), size(vort_pos,2)) :: dx
      real, intent(out), optional        :: delta
      real                               :: tmpdelta

      integer :: n_vort, i, j
      !vortex index vector -- which vortices are still there
      integer, dimension(size(vort_circ)) :: vort_iv
      real, dimension(size(vort_pos,1),size(vort_pos,2)) :: tmp_vort_v

      dx = 0
      tmp_vort_v = 0;

      n_vort = sum(abs(vort_circ))
      j = 0;
      do i=1,size(vort_circ)
         if(vort_circ(i)/=0) then
            j = j+1
            vort_iv(j) = i;
         end if
      end do

      call compute_vortex_velocities(vort_pos(:,vort_iv(1:j)),&
           &vort_circ(vort_iv(1:j)), tmp_vort_v(:,1:j), tmpdelta)

      dx(:,vort_iv(1:j)) = timestep(dtinit, vort_pos(:,vort_iv(1:j)),&
           &f, tmp_vort_v(:,1:j), tmpdelta, dtused)
      
      if(present(delta)) then
         delta = tmpdelta
      endif
      vort_v(:,vort_iv(1:j)) = tmp_vort_v(:,1:j)

      contains
        function f(vpos)
          real, dimension(:,:) :: vpos
          real, dimension(size(vpos,1), size(vpos,2)) :: f, tmp
          
          call compute_vortex_velocities(vpos, vort_circ(vort_iv(1:j)), tmp)
          f = tmp
        end function f
        
    end function step_dx

    subroutine compute_vort_distmatrix(vort_pos, distmatrix)
      real, intent(in) :: vort_pos(:,:)
      real, intent(out) :: distmatrix(:,:,:)

      integer :: i, j, N
      real :: dx, dy, r2

      N = size(vort_pos,2)
      do i=1,N
         do j=1,N
            dx = vort_pos(1,j) - vort_pos(1,i)
            dy = vort_pos(2,j) - vort_pos(2,i)
            r2 = dx**2 + dy**2
            distmatrix(:,j,i) = [dx, dy, r2]
         end do
      end do
    end subroutine compute_vort_distmatrix

    subroutine compute_pv_dists(vort_pos, points, dists)
      real, intent(in), dimension(:,:) :: vort_pos, points
      real, intent(out), dimension(:)   :: dists

      real :: r2min, r2, dx, dy
      integer :: V,P,i,k
      V = size(vort_pos,2)
      P = size(points, 2)

      do i=1,P
         r2min = 1
         do k=1,V
            dx = abs(vort_pos(1,k) - points(1,i))
            dx = min(dx, 1-dx)
            dy = abs(vort_pos(2,k) - points(2,i))
            dy = min(dy, 1-dy)
            r2 = dx**2 + dy**2
            if(r2 < r2min) r2min = r2
         end do
         dists(i) = sqrt(r2min)
      end do
    end subroutine compute_pv_dists
    
    subroutine compute_velocities(pos, vort_circ, vort_pos, vs, delta)
      real, dimension(:,:), intent(in)                       :: pos
      real, dimension(:,:), intent(in)                       :: vort_pos
      integer, dimension(:), intent(in)                      :: vort_circ
      real, dimension(size(pos,1), size(pos,2)), intent(out) :: vs
      real, intent(out), optional                            :: delta
      real, dimension(2)                                     :: v
      integer                                                :: i,k, N, P, chunk
      real                                                   :: r

      real, dimension(size(pos,2)) :: tmpdeltas

      vs = get_evs(pos)
      tmpdeltas = 1
      N = size(vort_pos,2);
      P = size(pos,2);
      chunk = N/4
      !$omp parallel shared(N, P, pos, vort_pos, vort_circ, vs, tmpdeltas) private(i,k,v,r)
      !$omp do schedule(dynamic, chunk)
      do i = 1,P
         do k = 1,N
            v = vortex_field(pos(1,i), pos(2,i), &
                 vort_circ(k), vort_pos(1,k), vort_pos(2,k),r)

              vs(:,i) = vs(:,i) + v
              if(r < tmpdeltas(i)) then 
                 tmpdeltas(i) = r
              end if
            
         end do
      end do
      !$omp end do nowait
      !$omp end parallel
     
      if(present(delta)) then
         delta = minval(tmpdeltas)
      endif      

    end subroutine compute_velocities

    subroutine compute_vortex_velocities(vort_pos, vort_circ, vort_v, delta)
      real, dimension(:,:), intent(in)                                 :: vort_pos
      integer, dimension(:), intent(in)                                :: vort_circ
      real, intent(out), optional                                      :: delta
      real, dimension(size(vort_pos,1), size(vort_pos,2)), intent(out) :: vort_v
      real, dimension(size(vort_pos,1), size(vort_pos,2)) :: v_s, v_n

      real :: tmpdelta

      call compute_vort_s_vel(vort_pos, vort_circ, v_s, tmpdelta);

      if(mf) then
         v_n = get_evn(vort_pos)

         vort_v(1,:) = v_s(1,:) - alpha*vort_circ*(v_n(2,:) - v_s(2,:)) &
              &+ alphap*(v_n(1,:) - v_s(1,:))
         vort_v(2,:) = v_s(2,:) + alpha*vort_circ*(v_n(1,:) - v_s(1,:)) &
              &+ alphap*(v_n(2,:) - v_s(2,:))
      else
         vort_v = v_s;
      endif
      if(present(delta)) delta = tmpdelta
    end subroutine compute_vortex_velocities

    subroutine compute_vort_s_vel(vort_pos, vort_circ, vort_v, delta)
      real, dimension(:,:), intent(in)                                 :: vort_pos
      integer, dimension(:), intent(in)                                :: vort_circ
      real, intent(out), optional                                      :: delta
      real, dimension(size(vort_pos,1), size(vort_pos,2)), intent(out) :: vort_v

      real, dimension(2)              :: v !velocity due to single vortex
      integer                         :: i,k, N
      real                            :: r

      real, dimension(size(vort_pos,2),size(vort_pos,2)) :: tmpdeltas

      integer :: chunk

      N = size(vort_pos,2)
      vort_v = get_evs(vort_pos)

      tmpdeltas = 1;
      if(N > 10) then 
         chunk = N / 4;
      else
         chunk = N;
      end if
      
      !$omp parallel shared(N, vort_pos, vort_circ, vort_v, tmpdeltas) private(i,k,v,r)
      !$omp do schedule(dynamic, chunk)
      do i = 1,N
         vort_v(:,i) = vort_v(:,i) + shadow_vortex_field(vort_pos(1,i), vort_pos(2,i),&
              &vort_circ(i), vort_pos(1,i), vort_pos(2,i), r)
         tmpdeltas(i,i) = r

         do k = (i+1),N
            v = vortex_field(vort_pos(1,i), vort_pos(2,i), &
                 1, vort_pos(1,k), vort_pos(2,k), r)
            tmpdeltas(k,i) = r
    
            vort_v(:,i) = vort_v(:,i) + v*vort_circ(k)
            vort_v(:,k) = vort_v(:,k) - v*vort_circ(i)
         end do
      end do
      !$omp end do nowait
      !$omp end parallel

      if(present(delta)) then
         delta = minval(tmpdeltas)
      end if
    end subroutine compute_vort_s_vel
    
    function periodic_vortex_field(x, y, vortex_circ, vortex_x, vortex_y, r) result(v)
      real, intent(in)            :: x, y, vortex_x, vortex_y
      integer, intent(in)         :: vortex_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v

      integer, dimension(5), parameter :: circ_flips = [1,1,1,1,1]
      real, dimension(5), parameter :: &
           & xshift = [0.0, -1.0, 1.0, 0.0, 0.0],&
           & yshift = [0.0, 0.0, 0.0, -1.0, 1.0]

      real, dimension(5)    :: r2
      real, dimension(5,2) :: vtmp
      real, dimension(5)    :: dx, dy
      integer, dimension(5) :: circs

      circs = vortex_circ*circ_flips

      dx = x - vortex_x - xshift
      dy = y - vortex_y - yshift
      r2 = dx**2 + dy**2
      vtmp(:,1) = - circs/2.0/pi*dy/r2
      vtmp(:,2) =   circs/2.0/pi*dx/r2
      
      v(:) = sum(vtmp, 1)

      if(present(r)) then
         r = minval(sqrt(r2))
      endif
    end function periodic_vortex_field
    
    function xwall_vortex_field(x, y, vortex_circ, vortex_x, vortex_y, r) result(v)
      real, intent(in)            :: x, y, vortex_x, vortex_y
      integer, intent(in)         :: vortex_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v

      integer, dimension(5), parameter :: circ_flips = [1,-1,-1,1,1]
      real, dimension(5), parameter :: &
           & yshift = [0.0, 0.0, 0.0, -1.0, 1.0]

      real, dimension(5)    :: r2
      real, dimension(5,2) :: vtmp
      real, dimension(5)    :: dx, dy
      integer, dimension(5) :: circs

      circs = vortex_circ*circ_flips

      dx = x - [vortex_x, -vortex_x, 2-vortex_x, 0.0, 0.0]
      dy = y - vortex_y - yshift
      r2 = dx**2 + dy**2
      vtmp(:,1) = - circs/2.0/pi*dy/r2
      vtmp(:,2) =   circs/2.0/pi*dx/r2
      
      v(:) = sum(vtmp, 1)

      if(present(r)) then
         r = minval(sqrt(r2))
      endif
    end function xwall_vortex_field

    function calculate_energy(vort_circ, vort_pos, M) result(E)
      ! MxM grid ponints to calculate energy
      implicit none
      real, dimension(:,:), intent(in) :: vort_pos
      integer, dimension(:), intent(in) :: vort_circ
      integer, intent(in) :: M
      real :: E
      real, dimension(2,M) :: grid_line
      real, dimension(2,M) :: v
      integer :: i,j
      grid_line(1,:) = [((1.0*j)/M, j=1, M)]
      E = 0
      do i=1,M
         grid_line(2,:) = [((1.0*i)/M, j=1, M)]
         call compute_velocities(grid_line, vort_circ, vort_pos, v)
         E = E + sum(sqrt(v(1,:)**2 + v(2,:)**2))
      end do
      E = E/(M**2)
    end function calculate_energy

    subroutine set_mutual_friction(sa, sap)
      real, intent(in) :: sa, sap

      alpha = sa
      alphap = sap
      mf = .true.
    end subroutine set_mutual_friction
    


    !
    !   boundary conditions code
    !

    function shadow_vortex_positions(vort_x, vort_y) result (v)
      real, intent(in) :: vort_x, vort_y
      real, dimension(2, 4) :: v
      v = reshape([vort_x -1, vort_y, vort_x + 1, vort_y,&
           &vort_x, vort_y - 1, vort_x, vort_y + 1], [2,4]);
    end function shadow_vortex_positions

    !
    ! periodic boundaries
    !

    function periodic_shadow_vortex_field(x, y, vortex_circ, vortex_x, vortex_y, r) result(v)
      real, intent(in)            :: x, y, vortex_x, vortex_y
      integer, intent(in)         :: vortex_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v

      integer, dimension(4), parameter :: circ_flips = [1,1,1,1]
      real, dimension(4), parameter :: &
           & xshift = [-1.0, 1.0, 0.0, 0.0],&
           & yshift = [ 0.0, 0.0, -1.0, 1.0]

      real, dimension(4)    :: r2
      real, dimension(4,2) :: vtmp
      real, dimension(4)    :: dx, dy
      integer, dimension(4) :: circs

      circs = vortex_circ*circ_flips

      dx = x - vortex_x - xshift
      dy = y - vortex_y - yshift
      r2 = dx**2 + dy**2
      vtmp(:,1) = - circs/2.0/pi*dy/r2
      vtmp(:,2) =   circs/2.0/pi*dx/r2
      
      v(:) = sum(vtmp, 1)

      if(present(r)) then
         r = minval(sqrt(r2))
      endif
    end function periodic_shadow_vortex_field
        
    subroutine periodic_coerce(vort_pos)
      real, dimension(:,:), intent(inout) :: vort_pos

      integer :: i,j
      do j=1,size(vort_pos,2)
         do i=1,2
            if(vort_pos(i,j) > 1) then
               vort_pos(i,j) = vort_pos(i,j) - 1
            else if (vort_pos(i,j) < 0) then
               vort_pos(i,j) = vort_pos(i,j) + 1
            end if
         end do
      end do
    end subroutine periodic_coerce

    !
    !  solid wall boundaries in x-direction
    !
    function xwall_shadow_vortex_field(x, y, vortex_circ, vortex_x, vortex_y, r) result(v)
      real, intent(in)            :: x, y, vortex_x, vortex_y
      integer, intent(in)         :: vortex_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v

      integer, dimension(4), parameter :: circ_flips = [-1,-1,1,1]
      real, dimension(4), parameter :: &
           & yshift = [0.0, 0.0, -1.0, 1.0]

      real, dimension(4)    :: r2
      real, dimension(4,2) :: vtmp
      real, dimension(4)    :: dx, dy
      integer, dimension(4) :: circs

      circs = vortex_circ*circ_flips

      dx = x - [-vortex_x, 2-vortex_x, 0.0, 0.0]
      dy = y - vortex_y - yshift
      r2 = dx**2 + dy**2
      vtmp(:,1) = - circs/2.0/pi*dy/r2
      vtmp(:,2) =   circs/2.0/pi*dx/r2
      
      v(:) = sum(vtmp, 1)

      if(present(r)) then
         r = minval(sqrt(r2))
      endif
    end function xwall_shadow_vortex_field

    subroutine xwall_periodic_coerce(vort_pos)
      real, dimension(:,:), intent(inout) :: vort_pos
      
      integer :: j
      do j=1,size(vort_pos,2)
         if(vort_pos(2,j) > 1) then
            vort_pos(2,j) = vort_pos(2,j) - 1
         else if (vort_pos(2,j) < 0) then
            vort_pos(2,j) = vort_pos(2,j) + 1
         end if
      end do
    end subroutine xwall_periodic_coerce

end module Vortices
