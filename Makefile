export project_root = $(shell pwd)
export modules = $(project_root)/MODULES
export binaries = $(project_root)/bin
export f90 =  gfortran -Wall -Wextra -pedantic -O3 -J $(modules)\
	-fimplicit-none -fdefault-real-8 -fopenmp -march=native -mfpmath=sse -ggdb

export objects = Units.o Init.o Time_evolution.o Fileio.o Tests.o stepping.o\
	         Reconnections.o LinkedList.o Particles.o Vortices.o external_flow.o

all : $(objects)

Units.o : Units.f90
Init.o : Init.f90
Time_evolution.o : Time_evolution.f90 Vortices.o Particles.o
Fileio.o : Fileio.f90
Tests.o : Tests.f90
stepping.o : stepping.f90
Reconnections.o : Reconnections.f90 LinkedList.o
Particles.o : Particles.f90 external_flow.o
Vortices.o : Vortices.f90 stepping.o external_flow.o
external_flow.o : external_flow.f90

%.o : %.f90
	$(f90) $< -c

#executables
SnapField_objects = Fileio.o Vortices.o external_flow.o stepping.o Particles.o
SnapField : $(SnapField_objects)
	$(f90) SnapField.f90 $(SnapField_objects) -o $(binaries)/$@

VsDist_objects = $(SnapField_objects)
VsDist : $(VsDist_objects)
	$(f90) VsDist.f90 $(VsDist_objects) -o $(binaries)/$@

clean :
	rm -f $(objects) $(binaries)/* $(modules)/*\
	&& cd Tests/antipar_vort && make clean\
	&& cd ../vort_part && make clean\
	&& cd ../antipar_vort_mf && make clean\
	&& cd ../par_vort_spin && make clean\
	&& cd ../single_vortex_drift && make clean\
	&& cd ../stationary_vortex && make clean\
	&& cd ../Nstat_vortices && make clean\
	&& cd ../vortN && make clean\
	&& cd ../vort100_no_mf && make clean\
	&& cd ../vort100_rec_no_mf && make clean\
	&& cd ../vort200_rec_mf && make clean\
	&& cd ../vort200_rec_no_mf && make clean\
	&& cd ../decay && make clean\
	&& cd ../vortex_clusters && make clean\
	&& cd $(project_root)

binclean : 
	rm -f $(binaries)/*

old :
	cd Tests/vort200_rec_no_mf && make\
	&& cd ../vort100_no_mf && make\
	&& cd ../vort100_rec_no_mf && make\
	&& cd ../vort200_rec_mf && make

basic_tests : 
	cd Tests/vort1_part1 && make\
	&& cd ../antipar_vort_mf && make\
	&& cd ../par_vort_spin && make\
	&& cd ../single_vortex_drift && make\
	&& cd ../stationary_vortex && make\
	&& cd ../Nstat_vortices && make\
	&& cd ../xwalls_test && make\
	&& cd $(project_root)

decay : 
	cd Tests/decay && make\
	&& cd ../decay_noframes && make\
	&& cd ../vortex_clusters && make\
	&& cd $(project_root)

vort_part :
	cd Tests/vort_part && make\
	&& cd $(project_root)

vortN : 
	cd Tests/vortN && make\
	&& cd $(project_root)

vortN_gridinit :
	cd Tests/vortN_gridinit && make\
	&& cd $(project_root)

vortN_xwalls : 
	cd Tests/vortN_xwalls && make\
	&& cd $(project_root)

vortN_ab : 
	cd Tests/vortN_ab && make\
	&& cd $(project_root)

large_eddy :
	cd Tests/large_eddy && make\
	&& cd $(project_root)

full :
	make vortN && make vort_part && make basic_tests && make SnapField && make VsDist\
	&& make vortN_gridinit && make vortN_xwalls
