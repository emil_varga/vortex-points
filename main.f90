program vortex_points
  use Units
  use Init
  use Time_evolution
  use FileIO
  use Tests
  implicit none

  integer, parameter :: N = 2 !number of vortex points
  integer, parameter :: Nt = 300 !number of frames
  real, parameter    :: dt = 1e-2, end_time = Nt * dt
  !external velocities
  real, parameter    :: exvsx = 0, exvsy = 0

  real, dimension(N)    :: vort_x, vort_y !vortex points coordinates
  real, dimension(N)    :: vort_vx, vort_vy !vortex points velocities
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)
  real                  :: time = 0
  integer               :: i = 0!frame
  !vortex positions and time are in non-dimensional units
  
  !call initialize_random(vort_circ, vort_x, vort_y, N)

  ! manual initialization
  vort_y = [0.5, 0.5]
  vort_x = [0.4, 0.6]
  vort_circ = [-1,1]

  call set_vsexternal(exvsx, exvsy)

  call boundary_vel('start-', vort_circ, vort_x, vort_y)
  do
     if (time > end_time) exit
     
     call compute_vortex_velocities(vort_x, vort_y, vort_circ, vort_vx, vort_vy, N)
     call save_snapshot(i, time, 'test'&
                       ,vort_x, vort_y, vort_circ, vort_vx, vort_vy, N)
     call step_advance(dt, vort_x, vort_y, vort_circ, vort_vx, vort_vy, N)

     time = time + dt
     i = i+1
  end do
  call boundary_vel('end-', vort_circ, vort_x, vort_y)
end program vortex_points
