program stat_vortex
  use Tests
  implicit none

  integer, parameter :: N = 4 !number of vortex points

  real, dimension(2,N)    :: vort_pos !vortex points coordinates
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)

  vort_pos = 0.5
  vort_circ = 0;
  call boundary_vel('no-vortex-', vort_circ, vort_pos)

  vort_circ = [1, 0, 0, 0]
  call boundary_vel('center-', vort_circ, vort_pos)

  vort_pos(:,1) = [0.3, 0.3]
  call boundary_vel('diag-', vort_circ, vort_pos)

  vort_pos =reshape([[0.3, 0.5], [0.7, 0.5], [.0,.0], [.0,.0]], [2,N])
  vort_circ = [1, 1, 0, 0]
  call boundary_vel('2-center-par-', vort_circ, vort_pos)

  vort_circ = [1, -1, 0, 0]
  call boundary_vel('2-center-antipar-', vort_circ, vort_pos)

  vort_pos = reshape([0.3, 0.3, 0.7, 0.7, 0.3, 0.7, 0.7, 0.4], [2,N])
  vort_circ = [1, 1, 1, 1]
  call boundary_vel('4-par', vort_circ, vort_pos)
  
  vort_circ = [1, 1, -1, -1]
  call boundary_vel('4-antipar', vort_circ, vort_pos)

end program stat_vortex
