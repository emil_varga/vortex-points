program xwalls_test
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  use Particles
  use Time_evolution
  use omp_lib
  implicit none

  !calculation parameters
  integer            :: N !number of vortex points
  real    :: rec_l  !reconnection length
  real, parameter    :: dtinit0 = 1e-2
  real :: dtinit = dtinit0

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v, dx
  integer, dimension(:), allocatable   :: vort_circ, anhs
  real                  :: time = 0, sc = 0, gc=0, dtused, delta_vv
  integer               :: i = 0, k=0, snaps, recs, gi
  character(len=6), dimension(3) :: names = ['1-test', '2-test', '3-test']

  integer :: cosprof_periods
  real :: cosprof_ampl

  !misc
  integer :: num_args
  real cpu_t0, cpu_t
  
  i=0
  k=0
  
  num_args = get_arguments(N, vsx, vnx, alpha, alphap,&
      &cosprof_ampl=cosprof_ampl, cosprof_periods=cosprof_periods,&
      &rec_l=rec_l)

  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif

  N = 4;

  allocate(vort_pos(2,N), dx(2,N), vort_v(2,N), vort_circ(N), anhs(N))

  print *, 'Initialzing...'
  vort_pos(:,1) = [0.05, 0.1];
  vort_pos(:,2) = [0.8, 0.3];
  vort_pos(:,3) = [0.99, 0.55];
  vort_pos(:,4) = [0.99, 0.45];
  vort_circ = [1, 0, 0,0];

  open(unit=3, file='timesteps.dat', status='NEW') 
  call set_vsexternal(vsx, vnx)
  !TODO: resuming on particles
  call set_mutual_friction(alpha, alphap)
  call set_cos_vnprofile(cosprof_periods, cosprof_ampl)

  !turn on the walls
  call set_vortex_field_function(xwall_vortex_field)
  call set_shadow_vortex_field_function(xwall_shadow_vortex_field)

  gi=0
  gc = 0
  snaps = 0
  cpu_t0 = omp_get_wtime();
  dtused = dtinit0
  print *, 'Starting'
  recs = 0
  do
     if(recs > 0) exit

     if(snaps < 3) then 
        call save_snapshot(i, time, names(snaps+1), vort_pos, vort_circ, vort_v)
        snaps = snaps + 1
     else
        i = i+1
        sc = 0
        snaps = 0
        cpu_t = omp_get_wtime();
        print *, i, 'Time: ', time, 'CPU time for frame:', cpu_t - cpu_t0
        cpu_t0 = cpu_t
     endif
       
     !align the frames
     dtinit = dtinit0
          
     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta_vv)

     vort_pos = vort_pos + dx
     call xwall_periodic_coerce(vort_pos)
     recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs, xwallp=.true.)

     write (3,*) k, dtused, recs, delta_vv

     time = time + dtused
     sc = sc + dtused
     k = k+1
  end do

  close(3)
  deallocate(vort_pos, dx, vort_v, vort_circ, anhs)

end program xwalls_test
