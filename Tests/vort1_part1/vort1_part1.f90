program vort1_part1
  use Vortices
  use Particles
  use Time_evolution
  use FileIO

  real, parameter :: tau_visc = 2.9241e-1, rho0 = 1.5, rhos = 0.268, rhon = 0.732

  integer, parameter :: V=1, P=1 !number of vortices and particles

  real, dimension(2,V)  :: vort_pos, vort_vel=0, dx=0
  integer, dimension(V) :: vort_circ, trapped(P)
  real, dimension(2, 2*P) :: part_posvel, part_velacc=0, dxp=0

  real :: time = 0, sc = 0, dtinit, dtused, delta_vv, delta_vp
  integer :: frame

  vort_pos = reshape([0.5, 0.5], [2,V])
  vort_circ = [1]
  part_posvel = reshape([0.7, 0.5, 0.0, 0.0], [2, 2*P])

  call set_particle_params(tau_visc, rho0, rhos, rhon)
  trapped = 0

  dtinit = 1e-4
  frame = 0
  open(unit=3, file='timesteps.dat', status='NEW')

  call particle_vort_step_dx(dtinit, part_posvel, trapped, vort_pos, vort_circ,&
       &dx, dxp, vort_vel, part_velacc, dtused, delta_vv, delta_vp)
  call save_snapshot(frame, time, 'vort-', vort_pos, vort_circ, vort_vel)
  call save_snapshot_particles(frame, time, 'particles-', part_posvel, part_velacc, trapped)
  frame = frame + 1
  delta_vp = 1
  do
     if(time > 1 .or. delta_vp < 1e-5) exit

     call particle_vort_step_dx(dtinit, part_posvel, trapped, vort_pos, vort_circ,&
          &dx, dxp, vort_vel, part_velacc, dtused, delta_vv, delta_vp)

     if(sc > 1e-3) then
        sc = 0
        call save_snapshot(frame, time, 'vort-', vort_pos, vort_circ, vort_vel)
        call save_snapshot_particles(frame, time, 'particles-', part_posvel, part_velacc, trapped)
        frame = frame + 1
     end if
     
     write (3,*) time, dtused, 0, delta_vv, delta_vp
     time = time + dtused
     sc   = sc   + dtused

     vort_pos = vort_pos + dx
     part_posvel = part_posvel + dxp
  end do
  close(3)
end program vort1_part1
