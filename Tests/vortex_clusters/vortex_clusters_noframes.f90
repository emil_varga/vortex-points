program vortex_clusters_noframes
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  implicit none

  !calculation parameters
  integer            :: N !number of vortex points
  real    :: rec_l = 2.0e-4!reconnection length
  real, parameter    :: dtinit = 1e-2

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v, dx
  integer, dimension(:), allocatable   :: vort_circ, anhs
  real                  :: time = 0, dtused, delta_vv
  integer               :: i, k, recs
  integer               :: Nvort !number of surviving vortex points

  integer :: num_args

  integer, parameter :: min_eddy = 100, max_eddy = 500
  integer :: eddy_strength
  real :: xc, yc, s, es_r

  num_args = get_arguments(N, vsx, vnx, alpha, alphap, rec_l = rec_l)
  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif
  allocate(vort_pos(2,N), dx(2,N), vort_v(2,N), vort_circ(N), anhs(N))
  vort_circ(1:(N/2)) = 1;
  vort_circ((N/2+1):N) = -1;
  
  do k=0,1
     i = 1;
     do
        if(N/2 - i < max_eddy) then
           eddy_strength = N/2 - i + 1;
        else
           call random_number(es_r);
           eddy_strength = floor(min_eddy + (max_eddy-min_eddy)*es_r);
        end if
        call random_number(xc);
        call random_number(yc);
        call random_number(s);
        s = s*0.5;
        call make_cluster_gaussian(xc, yc, s, &
             &vort_pos(:,(k*N/2+i):(k*N/2+i+eddy_strength-1)))
        
        i = i+eddy_strength
        if(i > N/2) exit;
     end do
  end do
  
  call periodic_coerce(vort_pos)
  call set_vsexternal(vsx, vnx)
  call set_mutual_friction(alpha, alphap)

  open(unit=3, file='timesteps.dat', status='NEW')
  Nvort = N
  call save_snapshot(0, time, 'vort-pos-', vort_pos, vort_circ, vort_v)
  
  recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs, .true.)
  !number of anihilated vortices is twice the number of reconections
  Nvort = Nvort - 2*recs
  k=0
  do
     if(Nvort == 0) exit

     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta_vv);
     vort_pos = vort_pos + dx
     
     call periodic_coerce(vort_pos)
  
     recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs, .true.)
     !number of anihilated vortices is twice the number of reconections
     Nvort = Nvort - 2*recs
     
     write (3,*) k, dtused, Nvort, recs, delta_vv
     time = time + dtused
     k = k+1
  end do

  close(3)
  close(2)
  deallocate(vort_pos, dx, vort_v, vort_circ, anhs)
end program vortex_clusters_noframes
