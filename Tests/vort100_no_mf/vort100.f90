program vort100
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  implicit none

  integer, parameter :: N = 100 !number of vortex points
  real, parameter    :: dtinit = 1e-2, end_time = 1

  integer, parameter :: Ms = 1000
  
  real, dimension(2,N)    :: vort_pos,dx !vortex points coordinates
  real, dimension(2,N)    :: vort_v !vortex points velocities
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)
  real                  :: time = 0, ec=0, sc = 0, dtused, delta
  integer               :: i = 0, k=0, m=0
  real                  :: energy

  integer, parameter :: npdf = 1000
  real, dimension(2, npdf*npdf) :: grid
  real, dimension(2, npdf*npdf) :: vels = 0
  integer, dimension(npdf*npdf) :: dummy = 0

  grid = reshape([(([(1.0*i)/npdf, (1.0*k)/npdf], k=1,npdf), i=1,npdf)], [2, npdf*npdf])

  
  call initialize_random(vort_circ, vort_pos)

  open(unit=2, file='energy.dat', status = 'NEW')
  open(unit=3, file='timesteps.dat', status='NEW')
  i=0
  k=0
  call compute_velocities(grid, vort_circ, vort_pos, vels)
  call save_snapshot(k, time, 'velocity-stats-start-', grid, dummy, vels)
  do
     if(time > end_time) exit
     if(ec>0.1) then
        ec = 0
        energy = calculate_energy(vort_circ, vort_pos, Ms)
        write (2,*) time, energy
     endif

     if(sc>0.001) then
        sc = 0
        call save_snapshot(i, time, 'test', vort_pos, vort_circ, vort_v)
        i = i+1
        print *, 'Time: ', time
     endif
     
     if(time>0.5 .and. m < 3) then
        call compute_velocities(grid, vort_circ, vort_pos, vels)
        call save_snapshot(m, time, 'velocity-stats-mid-', grid, dummy, vels)
        m = m+1
     end if

     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta)
     vort_pos = vort_pos + dx
     call periodic_coerce(vort_pos)

     write (3,*) k, dtused, delta

     time = time + dtused
     ec = ec + dtused
     sc = sc + dtused
     k = k+1
  end do
  call compute_velocities(grid, vort_circ, vort_pos, vels)
  call save_snapshot(k, time, 'velocity-stats-end-', grid, dummy, vels)

  close(2)
  close(3)
end program vort100
