program vortN_gridinit
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  use Particles
  use Time_evolution
  use omp_lib
  implicit none

  !how often to save frames
  real, parameter :: frame_interval = 1e-4

  !calculation parameters
  integer            :: N !number of vortex points
  real    :: rec_l  !reconnection length
  real, parameter    :: dtinit0 = 1e-2, end_time = 1
  real :: dtinit = dtinit0

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v, dx
  real, dimension(:), allocatable      :: vp_dists
  integer, dimension(:), allocatable   :: vort_circ, anhs
  real                  :: time = 0, sc = 0, gc=0, dtused, delta_vv
  integer               :: i = 0, k=0, snaps, recs, gi, sgn
  character(len=6), dimension(3) :: names = ['1-test', '2-test', '3-test']

  integer :: cosprof_periods
  real :: cosprof_ampl

  !misc
  integer :: num_args
  real cpu_t0, cpu_t
  character(len=32) :: arg
  character(len=128) :: filename
  
  i=0
  k=0
  
  num_args = get_arguments(N, vsx, vnx, alpha, alphap,&
      &cosprof_ampl=cosprof_ampl, cosprof_periods=cosprof_periods,&
      &rec_l=rec_l)

  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif
  allocate(vort_pos(2,N), dx(2,N), vort_v(2,N), vort_circ(N), anhs(N))

  if(num_args > 1) then
     call get_command_argument(2, filename)
     print *, 'Resuming from ', filename
     if(load_snapshot(filename, vort_circ, vort_pos, vort_v, time, N) < 0) then
        print *, 'Error loading snapshot'
        call exit(-1)
     end if
     call get_command_argument(3, arg)
     read (arg, *) i
     open(unit=3, file='timesteps.dat', status='OLD', position='append')
     print *, 'resumed from time = ', time, 'i = ', i
  else
     print *, 'Initialzing to regular positions on a grid...'
     gi = int(sqrt(real(N))) + 1
     sgn = 1
     i=0; k=1;
     do
        if(gi*i + k > N) exit
        vort_pos(:,gi*i + k) = [real(i+1)/real(gi + 1), real(k)/real(gi+1)]
        vort_circ(gi*i+k) = sgn;

        sgn = -sgn;
        if(k==gi) then
           k = 1
           i = i+1
        else
           k = k + 1
        end if
     end do
     
     open(unit=3, file='timesteps.dat', status='NEW')
  end if
  
  call set_vsexternal(vsx, vnx)
  !TODO: resuming on particles
  call set_mutual_friction(alpha, alphap)
  call set_cos_vnprofile(cosprof_periods, cosprof_ampl)

  gi=0
  gc = 0
  snaps = 0
  cpu_t0 = omp_get_wtime();
  dtused = dtinit0
  i = 0
  k = 0

  print *, 'Starting'
  do
     if(time > end_time) exit

     if(sc >= frame_interval .or. (frame_interval-sc)/frame_interval < 0.01) then
        if(snaps < 3) then 
           call save_snapshot(i, time, names(snaps+1), vort_pos, vort_circ, vort_v)
           snaps = snaps + 1
        else
           i = i+1
           sc = 0
           snaps = 0
           cpu_t = omp_get_wtime();
           print *, i, 'Time: ', time, 'CPU time for frame:', cpu_t - cpu_t0
           cpu_t0 = cpu_t
        endif
     endif
     
     !align the frames
     dtinit = dtinit0
          
     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta_vv)

     vort_pos = vort_pos + dx
     call periodic_coerce(vort_pos)
     recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs)

     write (3,*) k, dtused, recs, delta_vv

     time = time + dtused
     sc = sc + dtused
     k = k+1
  end do

  close(3)
  deallocate(vort_pos, dx, vort_v, vort_circ, anhs, vp_dists)

end program vortN_gridinit
