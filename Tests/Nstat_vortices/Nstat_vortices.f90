program Nstat_vortex
  use Tests
  use Init
  implicit none

  integer, parameter :: N = 1000 !number of vortex points

  real, dimension(2,N)    :: vort_pos !vortex points coordinates
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)

  call initialize_random(vort_circ, vort_pos)
  call boundary_vel('N-stat-', vort_circ, vort_pos)

end program Nstat_vortex
