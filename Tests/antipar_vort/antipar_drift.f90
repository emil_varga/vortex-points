program vortex_spinning
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  implicit none

  integer, parameter :: N = 2 !number of vortex points
  real, parameter    :: dtinit = 1e-2, end_time = 3

  real, dimension(2,N)  :: vort_pos, dx !vortex points coordinates
  real, dimension(2,N)  :: vort_v !vortex points velocities
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)
  real                  :: time = 0, dtused
  integer               :: i = 0!frame
  !vortex positions and time are in non-dimensional units
  
  !call initialize_random(vort_circ, vort_x, vort_y, N)

  ! manual initialization

  vort_pos = reshape([[0.6, 0.4], [0.7, 0.3]], [2,N])
  vort_circ = [1, -1]

  call boundary_vel('start-', vort_circ, vort_pos)
  do
     if (time > end_time) exit
     
     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused);
     call save_snapshot(i, time, 'test', vort_pos, vort_circ, vort_v)
     vort_pos = vort_pos + dx
     call periodic_coerce(vort_pos)

     time = time + dtused
     i = i+1
  end do
  call boundary_vel('end-', vort_circ, vort_pos)
end program vortex_spinning
