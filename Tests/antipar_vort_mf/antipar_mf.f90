program antipar_mf
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  implicit none

  integer, parameter :: N = 2 !number of vortex points
  real, parameter :: rec_l = 2e-3
  real :: dtinit = 1e-3

  !external velocities, corresponding to P = 590W/m^2, L=15625 (l=80um) at T = 1.75K
  real, dimension(2), parameter :: vsx = [0.0, -20.722]
  real, dimension(2), parameter :: vnx = [0.0, 10.0]

  !mutual friction at T = 1.75K, from Donnelly tables
  real, parameter :: alpha = 0.10630, alphap = 0.0390

  real, dimension(2,N)  :: vort_pos, dx !vortex points coordinates
  real, dimension(2,N)  :: vort_v !vortex points velocities
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)
  real                  :: time = 0, dtused, sc=0
  integer               :: i = 0, recs=0

  vort_pos = reshape([[0.45, 0.5], [0.55, 0.5]], [2,N])
  vort_circ = [-1, 1]

  call set_vsexternal([0.0,0.0], vnx)
  call set_mutual_friction(alpha, alphap)
  call save_snapshot(i, time, 'test', vort_pos, vort_circ, vort_v)
  do
     if (recs > 0) exit
     
     if (sc>1e-3) then
        call save_snapshot(i, time, 'test', vort_pos, vort_circ, vort_v)
        sc = 0
     endif

     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused);
     vort_pos = vort_pos + dx
     call periodic_coerce(vort_pos)
     recs = reconnect_simple(vort_pos, vort_circ, rec_l)

     time = time + dtused
     sc = sc + dtused
     i = i+1
  end do
end program antipar_mf
