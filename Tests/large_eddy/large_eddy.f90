program large_eddy
  use Units
  use Init
  use Vortices
  use FileIO
  use Reconnections
  use omp_lib
  implicit none

  !how often to save frames
  real, parameter :: frame_interval = 1e-4

  !calculation parameters
  integer         :: N     !number of vortex points
  real            :: rec_l !reconnection length
  real, parameter :: dtinit0 = 1e-2, end_time = 1
  real            :: dtinit = dtinit0

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v, dx
  real, dimension(:), allocatable      :: vp_dists
  integer, dimension(:), allocatable   :: vort_circ, anhs
  real                  :: time = 0, sc = 0, gc=0, dtused, delta_vv
  integer               :: i = 0, k=0, snaps, recs, gi
  character(len=6), dimension(3) :: names = ['1-test', '2-test', '3-test']

  integer :: cosprof_periods
  real :: cosprof_ampl

  !misc
  integer :: num_args
  real cpu_t0, cpu_t
  character(len=32) :: arg
  character(len=128) :: filename
  
  !reconnections
  integer :: nj, pj
  real :: dxj, dyj, r
  real, dimension(2, 2) :: tmp_rnd !for re-insertion

  print *, 'Starting'
  
  i=0
  k=0
  
  num_args = get_arguments(N, vsx, vnx, alpha, alphap,&
      &cosprof_ampl=cosprof_ampl, cosprof_periods=cosprof_periods,&
      &rec_l=rec_l)

  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif
  allocate(vort_pos(2,N), dx(2,N), vort_v(2,N), vort_circ(N), anhs(N))

  if(num_args > 1) then
     call get_command_argument(2, filename)
     print *, 'Resuming from ', filename
     if(load_snapshot(filename, vort_circ, vort_pos, vort_v, time, N) < 0) then
        print *, 'Error loading snapshot'
        call exit(-1)
     end if
     call get_command_argument(3, arg)
     read (arg, *) i
     open(unit=3, file='timesteps.dat', status='OLD', position='append')
     print *, 'resumed from time = ', time, 'i = ', i
  else
     print *, 'Initialzing to random positions in a gaussian cluster...'
     call make_cluster_gaussian(0.5, 0.5, 1.0, vort_pos)
     vort_circ(1:N/2) = 1;
     vort_circ((N/2 + 1) : N) = -1;
!     call initialize_random(vort_circ, vort_pos)
     open(unit=3, file='timesteps.dat', status='NEW')
  end if
  
  call set_vsexternal(vsx, vnx)
  !TODO: resuming on particles
  call set_mutual_friction(alpha, alphap)

  call set_evn(evn_eddy);
  call set_vortex_field_function(unbounded_vortex_field);
  call set_shadow_vortex_field_function(null_shadow);

  gi=0
  gc = 0
  snaps = 0
  cpu_t0 = omp_get_wtime();
  dtused = dtinit0
  do
     if(time > end_time) exit

     if(sc >= frame_interval .or. (frame_interval-sc)/frame_interval < 0.01) then
        if(snaps < 3) then 
           call save_snapshot(i, time, names(snaps+1), vort_pos, vort_circ, vort_v)
           snaps = snaps + 1
        else
           i = i+1
           sc = 0
           snaps = 0
           cpu_t = omp_get_wtime();
           print *, i, 'Time: ', time, 'CPU time for frame:', cpu_t - cpu_t0
           cpu_t0 = cpu_t
        endif
     endif
     
     !align the frames
     dtinit = dtinit0
          
     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta_vv)

     vort_pos = vort_pos + dx

     !recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs)

     do pj=1,(N/2)
        do nj=(N/2+1),N
           dxj = vort_pos(1, pj) - vort_pos(1, nj)
           dyj = vort_pos(2, pj) - vort_pos(2, nj)
           r = sqrt(dxj**2 + dyj**2)
           if(r < rec_l) then
              call random_number(tmp_rnd)
              vort_pos(1,[pj, nj]) = 0.5 + sqrt(-log(tmp_rnd(1,:))/2)*cos(2*3.14159*tmp_rnd(2,:));
              vort_pos(2,[pj, nj]) = 0.5 + sqrt(-log(tmp_rnd(1,:))/2)*sin(2*3.14159*tmp_rnd(2,:));
           end if
        end do
     end do
     
     write (3,*) k, dtused, recs, delta_vv

     time = time + dtused
     sc = sc + dtused
     k = k+1
  end do

  close(3)
  deallocate(vort_pos, dx, vort_v, vort_circ, anhs, vp_dists)

contains
  function unbounded_vortex_field(x, y, v_circ, v_x, v_y, r) result(v)
      real, intent(in)            :: x, y, v_x, v_y
      integer, intent(in)         :: v_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v

      real :: dx, dy, r2

      dx = x - v_x;
      dy = y - v_y;
      r2 = dx**2 + dy**2;

      v = [-v_circ/2.0/3.14159*dy/r2,&
           &v_circ/2.0/3.14159*dx/r2];
      
      if(present(r)) r = sqrt(r2);
  end function unbounded_vortex_field
    
  function null_shadow(x, y, vortex_circ, vortex_x, vortex_y, r) result(v)
      real, intent(in)            :: x, y, vortex_x, vortex_y
      integer, intent(in)         :: vortex_circ
      real, intent(out), optional :: r
      real, dimension(2) :: v

      v = 0;
      if(present(r)) r = 10;
  end function null_shadow

  function evn_eddy(pos) result (vn)
    real, dimension(:, :), intent(in) :: pos
    real, dimension(2, size(pos,2)) ::  vn

    real, dimension(size(pos,2)) :: r2;
    real :: s = 0.3, u;

    u = cosprof_ampl;

    r2 = (pos(1, :) - 0.5)**2 + (pos(2, :) - 0.5)**2;

    vn(1,:) = -exp(-r2/s**2)*(pos(2,:) - 0.5)*2/s*u;
    vn(2,:) =  exp(-r2/s**2)*(pos(1,:) - 0.5)*2/s*u;
  end function evn_eddy

end program large_eddy
