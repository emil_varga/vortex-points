program vort_part
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  use Particles
  use Time_evolution
  use omp_lib
  implicit none

  !how often to save frames
  real, parameter :: frame_interval = 1e-4

  !particle parameters
  real :: tau_visc = 2.9241e-4, rho0 = 1.5, rhos = 0.268, rhon = 0.732

  !calculation parameters
  integer            :: N, P !number of vortex points and particles
  real    :: rec_l  !reconnection length
  real    :: trap_l ! trapping length
  real, parameter    :: dtinit0 = 1e-2, end_time = 1
  real :: dtinit = dtinit0

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v, dx, part_posvel, part_velacc, dxp
  real, dimension(:), allocatable      :: vp_dists
  integer, dimension(:), allocatable   :: vort_circ, trapped, anhs
  real                  :: time = 0, timep, sc = 0, gc=0, dtused, delta_vv, delta_vp
  integer               :: i = 0, k=0, snaps, recs, traps, untraps, gi
  character(len=6), dimension(3) :: names = ['1-test', '2-test', '3-test']


  integer :: cosprof_periods
  real :: cosprof_ampl

  !misc
  integer :: num_args
  real cpu_t0, cpu_t
  character(len=32) :: arg
  character(len=128) :: filename
  
  print *, 'Starting'
  
  i=0
  k=0
  
  num_args = get_arguments(N, vsx, vnx, alpha, alphap&
       &,P, tau_visc, rhoN, rhoS, rho0, rec_l, trap_l,&
       &cosprof_ampl=cosprof_ampl, cosprof_periods=cosprof_periods)
  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif
  allocate(vort_pos(2,N), dx(2,N), vort_v(2,N), vort_circ(N), anhs(N))
  allocate(part_posvel(2, 2*P), part_velacc(2, 2*P), trapped(P), vp_dists(P), dxp(2, 2*P))

  if(num_args > 1) then
     call get_command_argument(2, filename)
     print *, 'Resuming from ', filename
     if(load_snapshot(filename, vort_circ, vort_pos, vort_v, time, N) < 0) then
        print *, 'Error loading snapshot'
        call exit(-1)
     end if
     call get_command_argument(3, arg)
     read (arg, *) i
     open(unit=3, file='timesteps.dat', status='OLD', position='append')
     print *, 'resumed from time = ', time, 'i = ', i
  else
     print *, 'Initialzing to random positions...'
     call initialize_random(vort_circ, vort_pos)
     open(unit=3, file='timesteps.dat', status='NEW')
  end if
  
  call set_vsexternal(vsx, vnx)
  !TODO: resuming on particles
  call initialize_random_particles(part_posvel, vnx)
  trapped = 0
  call set_mutual_friction(alpha, alphap)
  call set_particle_params(tau_visc, rho0, rhos, rhon)
  call set_cos_vnprofile(cosprof_periods, cosprof_ampl)

  gi=0
  gc = 0
  snaps = 0
  cpu_t0 = omp_get_wtime();
  dtused = dtinit0
  do
     if(time > end_time) exit

     if(sc >= frame_interval .or. (frame_interval-sc)/frame_interval < 0.01) then
        if(snaps < 3) then 
           call save_snapshot(i, time, names(snaps+1), vort_pos, vort_circ, vort_v)
           if(snaps==2) call save_snapshot_particles(i, time, 'part-', part_posvel,&
                                                    &part_velacc, trapped, vp_dists)
           snaps = snaps + 1
        else
           i = i+1
           sc = 0
           snaps = 0
           cpu_t = omp_get_wtime();
           print *, i, 'Time: ', time, 'CPU time for frame:', cpu_t - cpu_t0
           cpu_t0 = cpu_t
        endif
     endif
     
     !align the frames
     dtinit = dtinit0
          
     call particle_vort_step_dx(dtinit, part_posvel, trapped, vort_pos, vort_circ,&
          &dx, dxp, vort_v, part_velacc, dtused, delta_vv, delta_vp)

     vort_pos = vort_pos + dx
     part_posvel = part_posvel + dxp
     call periodic_coerce(vort_pos)
     call periodic_coerce(part_posvel(:,1:P))
     recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs)
     call untrap_particles(trapped, anhs(1:(2*recs)), untraps)
     traps = trap_particles(part_posvel, trapped, vort_pos, vort_v, trap_l, vp_dists)

     write (3,*) k, dtused, recs, delta_vv, delta_vp, traps, untraps

     time = time + dtused
     sc = sc + dtused
     k = k+1
  end do

  close(3)
  deallocate(part_posvel, part_velacc, trapped, dxp)
  deallocate(vort_pos, dx, vort_v, vort_circ, anhs, vp_dists)

end program vort_part
