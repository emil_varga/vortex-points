program vort200_rec_mf
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  implicit none

  !calculation parameters
  integer, parameter :: N = 200 !number of vortex points
  real, parameter    :: dtinit = 1e-2, end_time = 1
  real, parameter    :: rec_l = 2.2e-3!reconnection length

  !external velocities, corresponding to P = 590W/m^2, L=15625 (l=80um) at T = 1.75K
  real, dimension(2), parameter :: vsx = [0.0, -20.722]
  real, dimension(2), parameter :: vnx = [0.0, 56.48]

  !mutual friction at T = 1.75K, from Donnelly tables
  real, parameter :: alpha = 0.10630, alphap = 0.0390


  !grid for energy calculation
  integer, parameter :: Ms = 1000
  
  !calculation variables
  real, dimension(2,N)    :: vort_pos,dx !vortex points coordinates
  real, dimension(2,N)    :: vort_v !vortex points velocities
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)
  real                  :: time = 0, ec=0, sc = 0, dtused, delta
  integer               :: i = 0, k=0, m=0, snaps, recs
  real                  :: energy
  character(len=6), dimension(3) :: names = ['1-test', '2-test', '3-test']

  !distributions
  integer, parameter :: npdf = 1000
  real, dimension(2, npdf*npdf) :: grid
  real, dimension(2, npdf*npdf) :: vels
  integer, dimension(npdf*npdf) :: dummy = 0

  grid = reshape([(([(1.0*i)/npdf, (1.0*k)/npdf], k=1,npdf), i=1,npdf)], [2, npdf*npdf])
!  names = reshape(['1-test', '2-test', '3-test'], [6,3]);
  
  call initialize_random(vort_circ, vort_pos)
  call set_vsexternal(vsx, vnx)
  call set_mutual_friction(alpha, alphap)

  open(unit=2, file='energy.dat', status = 'NEW')
  open(unit=3, file='timesteps.dat', status='NEW')
  i=0
  k=0
  snaps = 0
  call compute_velocities(grid, vort_circ, vort_pos, vels)
  call save_snapshot(k, time, 'velocity-stats-start-', grid, dummy, vels)
  do
     if(time > end_time) exit
     if(ec>0.1) then
        ec = 0
        energy = calculate_energy(vort_circ, vort_pos, Ms)
        write (2,*) time, energy
     endif

     if(sc>0.001) then
        if(snaps < 3) then 
           call save_snapshot(i, time, names(snaps+1), vort_pos, vort_circ, vort_v)
           snaps = snaps + 1
        else
           i = i+1
           sc = 0
           snaps = 0
           print *, 'Time: ', time
        endif
     endif
     
     if(time>0.5 .and. m < 3) then
        call compute_velocities(grid, vort_circ, vort_pos, vels)
        call save_snapshot(m, time, 'velocity-stats-mid-', grid, dummy, vels)
        m = m+1
     end if

     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta)
     vort_pos = vort_pos + dx
     call periodic_coerce(vort_pos)

     recs = reconnect_simple(vort_pos, vort_circ, rec_l)
     write (3,*) k, dtused, delta, recs

     time = time + dtused
     ec = ec + dtused
     sc = sc + dtused
     k = k+1
  end do
  call compute_velocities(grid, vort_circ, vort_pos, vels)
  call save_snapshot(k, time, 'velocity-stats-end-', grid, dummy, vels)

  close(2)
  close(3)
end program vort200_rec_mf
