program decay_noframes 
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  use Reconnections
  implicit none

  !calculation parameters
  integer            :: N !number of vortex points
  real    :: rec_l = 2.0e-3!reconnection length
  real, parameter    :: dtinit = 1e-2

  !external velocities
  real, dimension(2) :: vsx
  real, dimension(2) :: vnx

  !mutual friction
  real :: alpha, alphap

  !calculation variables
  real, dimension(:,:), allocatable    :: vort_pos, vort_v, dx
  integer, dimension(:), allocatable   :: vort_circ, anhs
  real                  :: time = 0, sc = 0, dtused, delta_vv
  integer               :: i = 0, k=0, recs
  integer               :: Nvort !number of surviving vortex points

  integer :: num_args

  num_args = get_arguments(N, vsx, vnx, alpha, alphap, rec_l = rec_l)
  if(num_args < 1) then
     print *, 'Missing name of file with parameters'
     call exit(-1)
  endif
  allocate(vort_pos(2,N), dx(2,N), vort_v(2,N), vort_circ(N), anhs(N))
  
  call initialize_random(vort_circ, vort_pos)
  call set_vsexternal(vsx, vnx)
  call set_mutual_friction(alpha, alphap)

  open(unit=3, file='timesteps.dat', status='NEW')
  i=0
  k=0
  Nvort = N
  do
     if(Nvort == 0) exit

     if(sc>0.0005) then
        sc = 0;
        i = i+1
        print *, i, ' Time: ', time, ' Nvort: ', Nvort
     endif
     
     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused, delta_vv);
     vort_pos = vort_pos + dx
     
     call periodic_coerce(vort_pos)
     
     recs = reconnect_simple(vort_pos, vort_circ, rec_l, anhs, .true.)
     !number of anihilated vortices is twice the number of reconections
     Nvort = Nvort - 2*recs
     
     write (3,*) k, dtused, Nvort, recs, delta_vv
     time = time + dtused
     sc = sc + dtused
     k = k+1
  end do

  close(3)
  close(2)
  deallocate(vort_pos, dx, vort_v, vort_circ, anhs)
end program decay_noframes
