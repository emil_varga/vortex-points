program vortex_spinning
  use Units
  use Init
  use Vortices
  use FileIO
  use Tests
  implicit none

  integer, parameter :: N = 2 !number of vortex points
  real, parameter    :: dtinit = 1e-2, end_time = 3

  integer, parameter, dimension(3) :: Ms = [100, 500, 1000]
  
  real, dimension(2,N)    :: vort_pos,dx !vortex points coordinates
  real, dimension(2,N)    :: vort_v !vortex points velocities
  integer, dimension(N) :: vort_circ !vortex circulations (+/- 1)
  real                  :: time = 0, ec=0, dtused
  integer               :: i = 0, j
  real                  :: energy(3)

  ! manual initialization

  vort_pos = reshape([[0.45, 0.5], [0.55,0.5]], [2,N])
  vort_circ = [1, 1]


  open(unit=2, file='energy.dat', status = 'NEW')
  do
     if(time > end_time) exit
     if(ec>0.1) then
        ec = 0
        do j=1,3
           energy(j) = calculate_energy(vort_circ, vort_pos, Ms(j))
        enddo
        write (2,*) time, energy
     endif
     
     dx = step_dx(dtinit, vort_pos, vort_circ, vort_v, dtused)
     call save_snapshot(i, time, 'test', vort_pos, vort_circ, vort_v)
     vort_pos = vort_pos + dx
     call periodic_coerce(vort_pos)

     time = time + dtused
     i = i+1
     ec = ec + dtused
  end do

  close(2)
end program vortex_spinning
