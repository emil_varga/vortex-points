module stepping
  implicit none

  real, parameter :: dt_tolerance_fraction = 30
  !dt_tolerance_fraction/pi is approximately number of steps per orbit
  !for two parallel spinnig vortices, so 20 corresponds to about 6

  !current state of the Adams-Bashforth integrator
  type ab_state_t
     !N gives number of points; K the number of frames and Kstart
     !gives index of the oldest frame, increasing cyclicaly
     integer :: N, K, Kstart
     
     !frames, abf will have dimension(2, N, K)
     real, dimension(:,:,:), allocatable :: abf
     real, dimension(:), allocatable :: ab_coef !array of size K

  end type ab_state_t
  type(ab_state_t) :: ab_state

  real, dimension(5,5), parameter :: ab_coefs_all = reshape(&
       &[ [1.0,        0.0,       0.0,       0.0,        0.0],&
       &  [-0.5,       1.5,       0.0,       0.0,        0.0],&
       &  [5.0/12,    -4.0/3,     23.0/12,   0.0,        0.0],&
       &  [-3.0/8,     37.0/24,  -59.0/24,   55.0/24,    0.0],&
       &  [251.0/720, -637.0/360, 109.0/30, -1387.0/360, 1901.0/720] ]&
       &, [5,5])

  interface
     function timestep_interface(dtinit, ys, f, vs, delta, dtused) result(dy)
       real, intent(in) :: dtinit, delta
       real, dimension(:,:), intent(in) :: vs, ys
       real, intent(out), optional :: dtused
       real, dimension(size(ys,1), size(ys,2)) :: dy
       interface
          function f(y)
            real, dimension(:,:) :: y
            real, dimension(size(y,1), size(y,2)) :: f
          end function f
       end interface
     end function timestep_interface

     function f_interface(y) result(f)
       real, dimension(:,:) :: y
       real, dimension(size(y,1), size(y,2)) :: f
     end function f_interface
  end interface
  
  procedure(timestep_interface), pointer :: timestep => timestep_rk4

  private ab_state

  contains
!
! setters
!

    subroutine set_timestep_function(f)
      procedure(timestep_interface) :: f

      timestep => f
    end subroutine set_timestep_function
    
!
! Runge-Kutta code
!

    function timestep_rk4(dtinit, ys, f, vs, delta, dtused) result(dy)
      real, intent(in) :: dtinit, delta
      real, dimension(:,:), intent(in) :: vs, ys
      real, intent(out), optional :: dtused
      real, dimension(size(ys,1), size(ys,2)) :: dy
      procedure(f_interface) :: f
      
      real :: dt
      dt = choose_timestep_maxvel(dtinit, vs, delta)
      dy = rk4_step_dumb(dt, ys, f, vs)
      if(present(dtused)) then
         dtused = dt
      end if
    end function timestep_rk4

    pure function choose_timestep_maxvel(dtinit, vs, delta) result (dt)
      real :: dt
      real, intent(in) :: dtinit, delta
      real, intent(in), dimension(:,:) :: vs
      real :: vmax, xdt !xdt - the maximum dt we can tolerate
      
      vmax = sqrt(maxval(vs(1,:)**2 + vs(2,:)**2)) !O(n) operaion

      xdt    = delta/vmax/dt_tolerance_fraction
      dt = dtinit
      do
         if(dt < xdt) exit
         dt = dt/2
      end do
    end function choose_timestep_maxvel

    function rk4_step_dumb(dt, yn, f, dydt) result(dy)
      real, intent(in) :: dt
      real, dimension(:,:), intent(in) :: yn, dydt
      real, dimension(size(yn,1), size(yn,2)) :: dy
      procedure(f_interface) :: f
      real, dimension(size(yn,1), size(yn,2)) :: k2, k3, k4

      k2 = f(yn + 0.5*dt*dydt)
      k3 = f(yn + 0.5*dt*k2)
      k4 = f(yn + dt*k3)

      dy = dt/6.0*(dydt + 2*k2 + 2*k3 + k4)
    end function rk4_step_dumb

!
! Adams-Bashforth code
!

    function timestep_ab(dtinit, ys, f, vs, delta, dtused) result(dy)
      real, intent(in) :: dtinit, delta
      real, dimension(:,:), intent(in) :: vs, ys
      real, intent(out), optional :: dtused
      real, dimension(size(ys,1), size(ys,2)) :: dy
      procedure(f_interface) :: f

      dy = ab_step(dtinit, ys, f);
      if(present(dtused)) dtused = dtinit
    end function timestep_ab

    function ab_step(dt, yn, f) result(dy)
      real, intent(in) :: dt
      real, dimension(:,:) :: yn
      real, dimension(size(yn,1), size(yn,2)) :: dy
      procedure(f_interface) :: f

      integer :: i
      integer, dimension(ab_state%K) :: ixs
      ixs = [(i, i=1, ab_state%K)]

      ixs = cshift(ixs, ab_state%Kstart - 1)
      dy = 0;
      do i=1,ab_state%K
         dy = dy + dt*ab_state%ab_coef(i)*ab_state%abf(:,:,ixs(i))
      end do

      ab_state%abf(:,:,ab_state%Kstart) = f(yn + dy)
      ab_state%Kstart = ab_state%Kstart + 1
    end function ab_step

    subroutine ab_start(dt, yn0, f, ts_fileno)
      real, intent(in) :: dt, yn0(:,:)
      procedure(f_interface) :: f
      integer, intent(in) :: ts_fileno
      
      !TODO: finish this
    end subroutine ab_start

!
! initialization and de-initialization
!

    subroutine ab_init(dt, order, yn)
      real, intent(in) :: dt
      integer, intent(in) :: order
      real, dimension(:,:), intent(in) :: yn
      procedure(f_interface) :: f

      integer :: N, K
      K = order
      N = size(yn, 2)
      
      ab_state%K = order
      ab_state%Kstart = 1

      allocate(ab_state%ab_coef(K), ab_state%abf(2, N, K))

      ab_state%ab_coef = ab_coefs_all(:,K)
      ab_state%abf = 0

      print *, 'ab_coef=', ab_state%ab_coef
    end subroutine ab_init

    subroutine ab_deinit()
      if(allocated(ab_state%abf)) deallocate(ab_state%abf)
      if(allocated(ab_state%ab_coef)) deallocate(ab_state%ab_coef)
    end subroutine ab_deinit
    
end module stepping
