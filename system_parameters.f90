module system_parameters
  !general
  real :: rho, rho_s, rho_n

  !Particle parameters
  real :: visc_relax_time, rho0

end module system_parameters
